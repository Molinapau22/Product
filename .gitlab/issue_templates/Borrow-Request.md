## Borrow Request Overview

<!-- Outline the goals and reasoning supporting the Borrow Request -->

### Exit Criteria

<!-- Define the exit criteria expected from the Borrow Request -->

### Proposal

<!-- Define the ask:
  1. Which team will receive engineers
  2. Estimated length of engagement
  3. Propose teams to source from
  4. Define DRI
-->

<!-- Example
1. **3 Backend Engineers** need to be added to ________ group in order to meet reliability goal ABC.
1. Timeline: 3 milestones ending in milestone 14.5
1. The DRI for Borrow Request will be EM of ________ group.
-->

### Requirements

<!-- Define any specific requirements that are important, such as certain skillsets that are required -->

### Projects

<!-- Using a table, outline the project and proposed team members -->

<!-- Example
| Priorities | Project | Team Members |
|------------|---------|----------|
| Top Priority for ~"Engineering Allocation" | [Deliver Infradev Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=devops%3A%3Averify)| `List of Engineers here`
-->

### Current Staffing

<!-- Using a table, outline current staffing -->

<!-- Example
| Team | BE | FE | EM | SDET |
|------|-------|-----|----|------|
| Pipeline Execution | (4) `Engineer1` `Engineer2` | 0 | 1 | 0 |
| Pipeline Authoring | (1) `Engineer3` | 0 | 0 | 0 | 0 |
| Testing | (1) `Engineer4` | 0 | 0 | 0 | 0 |
| Runner | 2 | 0 | 0 | 0 | 0 |
| **Total** | **8** | **0** | **0** | **0** | **0** |
-->

### Staffing Request

<!-- Using a table, outline the request from priority teams -->

<!-- Example
| Team | BE | FE | EM | SDET |
|------|-------|-----|------|-----|
| Monitor | 2 | 0 |0 | 0 |
| Configure | 1 | 0 |0 | 0 |
| **Total** | **3** | **0** | **0** | **0** | **0** |
-->

## Process Overview

A Borrow Request consists of 3 distinct phases:

1. Proposal
1. Rollout
1. Retrospective

### Proposal Phase
Once it is decided to proceed with a Borrow Request, a new GitLab issue is opened using the Borrow Request issue template.
This template contains a checklist summarizing tasks to be completed. The goal of this checklist is greater Transparency, Efficiency and achieving strong Results.

Once the impacted teams are decided upon, it is critical that impacted Engineering Managers are notified as soon as possible to ensure they have the correct context and understand the goals and expected timeline/rollout plan for the Borrow Request.

Scope of transparency: Leadership + Engineering Managers

Tasks:
- [ ] Define business need
- [ ] Create and link to initial scope
- [ ] Decide on estimated timeline, including a tentative transition date
- [ ] Define resourcing needs (how many FEs and BEs, etc)
- [ ] Clearly define exit criteria for the Borrow Request

### Rollout Phase
Rolling out the Borrow Request requires clear communication and context sharing as well as the ability to dynamically move with urgency while keeping everyone organized. Incoming ICs need to be properly onboarded to their new role. This includes fully understanding the goals, context, scope, and timeline of the Borrow Request.

Scope of transparency: Leadership + EMs + ICs

Tasks:

Communicating the changes:

- [ ] Confirm receiving teams have been notified
- [ ] Confirm that impacted EMs have communicated directly their ICs
- [ ] Transition date, context, and project scope clearly communicated to all impacted members
- [ ] If possible, allow volunteers to come forward first to their EMs. Should no volunteers come forward, EMs select individual ICs.
- [ ] Schedule AMA with all impacted team members
- [ ] Announce the changes in relevant public Slack channels.


Onboarding Engineers:

- [ ] Onboarding buddies selected for incoming ICs
- [ ] ICs are able to review specific set of issues to be accomplished during the Borrow Request
- [ ] On the expected start date, ICs meet with onboarding buddies.
- [ ] PMs and/or EMs followup with all ICs to answer any remaining questions
- [ ] ICs attend relevant new stand-ups and meetings


### Retrospective Phase
Scope of transparency: Leadership + EMs + ICs

Tasks:
- [ ] Retrospective issue created
- [ ] All impacted individuals (PMs, EMs, ICs etc) invited to participate
- [ ] Hold sync retrospective call
- [ ] Update this process or the Product template based on any retrospective findings/actions

### Relevant Documents

* [Add Document](URL)
* [Add Document](URL)
* [Add Document](URL)
