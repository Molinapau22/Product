## Monthly Competitive Roadmap Review

1. Review competitor roadmaps
1. Share insights this issue
1. Follow up with individual groups/categories

## References
* GitHub: https://github.com/orgs/github/projects/4247/views/1
  * Changelog is available by clicking on `Menu` on the right-hand side
* Atlassian/BitBucket: https://www.atlassian.com/roadmap/cloud?selectedProduct=bitbucket
  * JiraAlign: https://www.atlassian.com/roadmap/cloud?selectedProduct=jiraAlign
  * Jira SW: https://www.atlassian.com/roadmap/cloud?selectedProduct=jsw
* PlanView: https://www.atlassian.com/roadmap/cloud?selectedProduct=jsw
* Gerrit: https://www.gerritcodereview.com/roadmap.html
* JetBrains: https://www.jetbrains.com/space/roadmap/
* Xray (Jira): https://docs.getxray.app/display/XRAY/Roadmap
* Azure DevOps: https://docs.microsoft.com/en-us/azure/devops/release-notes/features-timeline
* Deployment:
  * Waypoint: https://www.waypointproject.io/docs/roadmap 
  * Harness: https://ngdocs.harness.io/article/a621g4zx1c-harness-on-prem-release-notes-2021
  * Argo CD: https://argo-cd.readthedocs.io/en/stable/roadmap/
  * Octopus Deploy: https://octopus.com/company/roadmap
* Monitor:
  * Datadog
  * GrafanaLabs
* [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Competitive%20Roadmap%20Review)

## Opening
* [ ] Create Retrospective Thread - @joshlambert
* [ ] Set the issue due date for mid-month - @joshlambert

## Tasks 

### Section reviewers
Review for implications to your sections groups and direction. Provide a highlight comment and ping your team

* [ ] Dev: @david
* [ ] Ops: @kencjohnston 
* [ ] Sec: @hbenson
* [ ] Enablement: @joshlambert 

### Group reviewers

* [ ] Global Search: @JohnMcGuire
* [ ] Project Horse: @awthomas
    * https://github.com/orgs/github/projects/4247/views/1?filterQuery=label%3A%22github+ae%22

## Closing
* [ ] Make [updates to this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Competitive-Roadmap-Review.md) based on retrospective thread - @joshlambert

FYI @francispotter

/assign @david @kencjohnston @joshlambert @JohnMcGuire @hbenson

/label ~"Competitive Roadmap Review"
