# Summary

Every six months, ProdOps is responsible for ensuring milestones are in place in GitLab for at least 9 milestones in to the future.

- [Handbook page](https://about.gitlab.com/handbook/product/milestones/#product-milestone-creation)

# To Do

- [ ] [Create milestones on .org](https://about.gitlab.com/handbook/product/milestones/#step-1-org)
- [ ] [Create milestones on .com](https://about.gitlab.com/handbook/product/milestones/#step-2-com)

/assign @fseifoddini @brhea
