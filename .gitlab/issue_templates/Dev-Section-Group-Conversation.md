Be sure to review and follow the most up-to-date [Group Conversation](https://about.gitlab.com/handbook/people-group/group-conversations/#for-meeting-leaders) instructions.

## References
* **[Slides](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g156008d958_0_18)**

## Tasks
* [ ] Review [current guidance for GC presentations](https://about.gitlab.com/handbook/people-group/group-conversations/#presentation)
* [ ] Add a "Retrospective Thread" comment to this issue
* [ ] Block off calendar for the 30 minutes prior to the meeting
* [ ] Add the link to the slides to the GC Agenda Doc 24 hours in advance of the meeting
* [ ] Add the links to the GC Agenda and Slides to the #group-conversation slack channel, cross post to #company-announcements
* [ ] Consider recording a video 24 hours before hand
* [ ] After each GC [update this GC prep issue template](https://gitlab.com/gitlab-com/Product/edit/master/.gitlab/issue_templates/Group-Conversation-Prep.md) with any added items

**Overall updates**
* [ ] Update Eng Team - @timzallmann
* [ ] Update Product Team - @david
* [ ] Add Eng OKR Summary - @timzallmann
* [ ] Add UX OKR Summary - @mikelong @mvanremmerden 
* [ ] Add Quality OKR Summary - @at.ramya 
* [ ] Add Top Highlights Summary - All
* [ ] Update Accomplishments and What's Next? slides

**Manage**
* [ ] Overall Manage Review - @jeremy
* [ ] Access @mushakov 
* [ ] Import @hdelalic
* [ ] Compliance @mattgonzales
* [ ] Analytics @npost

**Plan**
* [ ] Overall Plan Review - @justinfarris
* [ ] Project Management @gweaver 
* [ ] Portfolio Management @kokeefe
* [ ] Certify @mjwood 

**Create**
* [ ] Overall Create Review - @jramsay
* [ ] Source Code @danielgruesso
* [ ] Gitaly @jramsay @derekferguson
* [ ] Editor @phikai
* [ ] Knowledge @cdybenko
* [ ] Static Site Editor @ericschurter
* [ ] Ecosystem @deuley
