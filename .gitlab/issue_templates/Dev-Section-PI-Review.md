## :book: References

* [Handbook Page](https://internal-handbook.gitlab.io/product/performance-indicators/dev-section/)
* [Meeting Notes](https://docs.google.com/document/d/1h8j9Qzhfyv4KRryYAM362jqdn5A0OiXX-DHS1aaY2Xs/edit)
* [PI .yml file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/master/data/performance_indicators/dev_section.yml)

## :dart: Intent
We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements.

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Adjust issue title - @mushakov
* [ ] Set a due date to this issue as 3 business days prior to the scheduled review - @mushakov
* [ ] Link state of the data issue - @mushakov
* [ ] Add a restrospective thread to this issue - @mushakov

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/master/data/performance_indicators/dev_section.yml) with:

* Update your `health:` and `instrumentation:` levels
* Update any `urls:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `urls:` to reference additional Sisense dashboards for more details
* Update `health:` `reasons:` with `Insight -` prefixed comments about insights gained from the last month of data
* Update `health:` `reasons:` with `Improvement -` prefixed comments about planned improvements to reach your goals over the next month
* If there have been recent updates to the definition, rationale, growth model, funnel highlight those and include those as `reasons:` under `health:`
* If you don't have a defined metric, rationale, growth model or funnel in the handbook reference issues for creating those and provide status on blockers in the `reasons:` under `instrumentation:`.

### :star: Expectations
1. Ensure your graph is refreshed
1. Ensure your graph looks correct and if it's not, understand why and change it
1. Have a comprehensive understanding of your graphs (understand history, challenges with the data, etc) and document directly in the .yml file
1. Ensure there are no graph labeling or title errors
1. Ensure you have a target set and you know how that target was set. If you can't have a target, in the target field, please explain why you don't have a target and when you would expect to have one.

#### Group Updates

**Section**
* [ ] Section TMAU - @david

**Manage**
* [ ] Overall Manage Review 
* [ ] Authentication and Authorization @hsutor
* [ ] Workspace @ogolowinski 
* [ ] Import @hdelalic
* [ ] Compliance @sam.white
* [ ] Optimize @hsnir1

**Plan**
* [ ] Overall Plan Review @mushakov
* [ ] Project Management @gweaver
* [ ] Portfolio Management @mushakov
* [ ] Certify @mjwood

**Create**
* [ ] Overall Create Review @sarahwaldner
* [ ] Source Code @tlinz
* [ ] Gitaly @mjwood
* [ ] Code Review @phikai
* [ ] Editor @ericschurter

**Ecosystem**
* [ ] Overall Ecosystem Review - @mushakov
* [ ] Foundations @cdybenko
* [ ] Integrations @g.hickman

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Dev-Section-PI-Review.md) based on the retrospective thread - @david
* [ ] Activate on any retrospective thread items - @david

/assign @david @mushakov @sarahwaldner @ogolowinski @hsutor @hdelalic @sam.white @stkerr @hsnir1 @gweaver @mjwood @tlinz @mjwood @phikai @ericschurter @cdybenko @g.hickman

