##  Proposal
<!-- Describe the proposal, including a link to the public issue with what specifically will be changing. -->

## Rationale
<!-- Include rationale for the move. This section can contain multiple points such as buyer-based tiering conformance, critical customer request, or other business drivers. Please include the proposed narrative with details -->


**Core**: PM recommendation is that this feature should be in Core. This feature is essential for an IC to do their work. Without this feature an IC cannot effectively do {explain the job to be done}. If GitLab does not support this feature, the IC would instead have to {explain what the next best alternative is}. In addition having this in core is great for adoption. We expect adoption to increase by {x%}.

**Premium**: PM recommendation is that this feature should be in Premium tier. This feature is essential for Teams to do their work. Without this feature a Team cannot effectively do {explain the job to be done}. If GitLab does not support this feature, the team would instead have to {explain what the next best alternative is}. This feature is not essential for ICs (or very small teams) because {explain why}. We expect this to be part of the {stage} monetization. We do not expect this to curtail adoption in any meaningful way. If this feature was in Core, we expect {x%} additional adoption. When the feature is a rarely used feature but of high importance for the teams, err on the side of Premium. We will always continue to listen to our community and we can decide to move it to core based on feedback from our community.

**Ultimate**: PM recommendation is that this feature should be in Ultimate tier. This feature is essential for organizations to do their work. Without this feature an organization cannot effectively do {explain the job to be done}. If GitLab does not support this feature, the organization would instead have to {explain what the next best alternative is}. We expect this to be part of the {stage} monetization. If this feature was in Premium, {we expect|we do not expect} an impact to premium -> ultimate conversion because {explain why}.



## Financial Model
<!-- Copy and adjust the Pricing Tier Adjustment Model Template adding relevant details for this move. Summarize the resulting output in this issue. -->
* [Pricing Tier Adjustment Revenue Model - TEMPLATE](https://docs.google.com/spreadsheets/d/1VulFAyL4mfkpNEH5-YeOJsfWqDBlWIShfCr8Fjf8ibo/edit)

## Target Customer Interviews
<!-- Determine a list of affected customers and prospects and conduct interviews where appropriate. For customers, the act of involving GitLab advocates in the decision making process will smooth any future tension around the decision. Summarize the results of those conversations in this section. -->

## Process
### Content Creation and CEO and EVP of Product Approval to Proceed
* [ ] Ensure alignment with your Stage, Section Product Leaders and VP of Product Management 
* [ ] Mention in the comments your Product Marketing counterpart, the CRO, the CMO, the EVP of Product, and the CEO.
* [ ] If the change has significant financial or business model impact, partner with your Finance business partner and/or other critical team members to put together a rough sketch of the estimated impact. Update the issue with details. It is the responsibility of the Product Manager to have accurately identified, quantified, justified and communicated the business impact of the change prior to the proposal being submitted for approval.
* [ ] Share proposal for approval from the EVP of Product and the CEO to proceed.

### Company and Sales Review
* [ ] Share this issue for feedback in #sales, #customer-success, and #product.
* [ ] Join the weekly CRO Leadership Call, explain the proposal, and direct them to the issue to provide feedback. Please ask the appropriate EBA for an invitation in #eba-team. If attending the meeting is not feasible, ping the CRO on the issue and ask them to add it to an upcoming CRO Leadership meeting agenda.

### Final Approval
* [ ] After feedback has been provided update your recommendation and assign to the CEO for final approval.
* [ ] (optional) Update 'features.yml' tier reference for relevant features.

### After Approval
* [ ] Reference the confidential issue in the public issue with a brief statement summarizing the result.
* [ ] Record a brief video to GitLab Unfiltered announcing the change and share in #sales, #customer-success, #product, and #whats-happening-at-gitlab. Please include a timeline for the change.
* [ ] Work with a [Sales Communication Manager](https://about.gitlab.com/job-families/sales/sales-communications-manager/) on a broader Sales/CS communication plan. This may include including the announcement in a [Field Flash](https://about.gitlab.com/handbook/sales/field-communications/field-flash-newsletter/) newsletter or announcing the change on a call.

/confidential
