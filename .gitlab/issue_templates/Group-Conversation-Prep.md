<!-- HOW TO USE THIS TEMPLATE

First, delete the headers pertaining to Sections (Ops, Dev, Enablement) that aren't relevant for your group conversaton.

-->
Be sure to review and follow the most up-to-date [Group Conversation](https://about.gitlab.com/handbook/people-group/group-conversations/#for-meeting-leaders) instructions.

## References
* **[Slides](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g156008d958_0_18)**

## Tasks (All)
* [ ] Review [current guidance for GC presentations](https://about.gitlab.com/handbook/people-group/group-conversations/#presentation)
* [ ] Add a "Retrospective Thread" comment to this issue
* [ ] Block off calendar for the 30 minutes prior to the meeting - [Ops Section Calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV82ZWVxZmVycjN1MWZyNzI2c3EzNTJjZDhuY0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
* [ ] Delete all content from previous GC, mark screenshots for update

## Tasks (Ops)

### 👋 Welcome Intro

* [ ] Update the [issue prep reference in notes for the Cover Slide](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g156008d958_0_18) to this issue, update date on presentation to date of GC
* [ ] Update the "Retrospective Thread" link in the [Content Overview](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g84a305d6e4_2_17) slide
* [ ] Update [Closing Slide](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g63b3372f1e_2_0) - @kencjohnston
* [ ] Add at least one Easter Egg - @kencjohnston 

### 🗺 Overview

* [ ] Check Overview Section - @sgoldstein @jmandell @nudalova

### 🆕 Updates
Some standard update slides for new team members, but also a place to bubble up themes or other exciting updates that will be covered in the intro/overview.

* [ ] Update [Team Member Updates slide](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g84779638bf_6_7)
  * [ ] @kencjohnston 
  * [ ] @sgoldstein
  * [ ] @jo_shih
  * [ ] @nudalova
  * [ ] @jmandell
* [ ] Think through what questions we are encouraging - if there aren't new update slides to do so
  * [ ] @kencjohnston 
  * [ ] @kbychu 
 
### :heart: Gratitude
* [ ] Update Gratitude Slide - @kencjohnston 
* [ ] Ping in Slack and on this issue for additional gratitude call-outs - @kencjohnston

### 😍 Exciting Things
Highlight items on your future roadmap you are particularly excited about.

* [ ] Add [things we're most excited about](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g63a7a349c9_0_19), link to issues or Epics
  * [ ] Verify:CI - @thaoyeager
  * [ ] Verify:Pipeline Authoring @dhershkovitch
  * [ ] Verify:Runner - @DarrenEastman 
  * [ ] Verify:Testing - @jheimbuck_gl
  * [ ] Package - @trizzi
  * [ ] Release:Progressive Delivery - @ogolowinski 
  * [ ] Release:Release Management - @jreporter
  * [ ] Configure - @nagyv-gitlab
  * [ ] Monitor:Health - @sarahwaldner
  * [ ] Review and Summarize - @kencjohnston @kbychu @jyavorska

### 🎉 Accomplishments
Chance to showcase great features, shipped, problems validated, maturity increments, etc.

* [ ] Update Accomplishments list
  * [ ] Verify:CI - @thaoyeager
  * [ ] Verify:Pipeline Authoring - @dhershkovitch 
  * [ ] Verify:Runner - @DarrenEastman 
  * [ ] Verify:Testing - @jheimbuck_gl
  * [ ] Package - @trizzi
  * [ ] Release:Progressive Delivery - @ogolowinski 
  * [ ] Release:Release Management - @jreporter 
  * [ ] Configure - @nagyv-gitlab
  * [ ] Monitor:Health - @sarahwaldner
* [ ] Update Community Contributions list
  * [ ] [Verify](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g5e41cce80e_6_0) - @darbyfrey
  * [ ] [Package](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g5e41cce80e_6_0) - @dcroft
  * [ ] [Release](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g5e41cce80e_6_0) - @dcroft
  * [ ] [Configure](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g5e41cce80e_3_0) - @nicholasklick
  * [ ] [Monitor](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g5e41cce80e_6_0) - @crystalpoole

### 📊 Metrics & OKRs
* [ ] Update [Product Metrics](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g7ac1e7e659_0_39) screenshot - update Highlights slide including screenshots of Graphs - @kencjohnston
* [ ] Update [Eng Metrics](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g5c2bb11302_0_33)
  * [ ] @sgoldstein
* [ ] Update Product OKR slide link and screenshot - @kencjohnston
* [ ] Update Eng OKR Summary
  * [ ] @sgoldstein 
* [ ] Update UX OKR Summary
  * [ ] @jmandell
  * [ ] @nudalova

### 🎬 Closing
* [ ] Update [Content Overview](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g658eb88d0f_2_19) slide - @kencjohnston
* [ ] Record a video 24 hours before hand
* [ ] Add the link to the slides to the [GC Agenda Doc](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit) 24 hours in advance of the meeting
* [ ] Post a message to #whats-happening-at-gitlab slack channel with a link to the video and threads with links to the [Slides](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g156008d958_0_18) and [GC Agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit)
* [ ] After each GC [update this GC prep issue template](https://gitlab.com/gitlab-com/Product/edit/master/.gitlab/issue_templates/Group-Conversation-Prep.md) with any added items
* [ ] Before closing this issue create a new one from the updated template and assign a due date for when the next GC is scheduled - @kencjohnston 

/assign @kencjohnston @kbychu @dhershkovtich @sarahwaldner @nagyv-gitlab @jyavorska @thaoyeager @DarrenEastman @jheimbuck_gl @trizzi @ogolowinski @jmeshell @nudalova @darbyfrey @sgoldstein @mnohr @nicholasklick @dcroft @jo_shih

# Tasks (Enablement)
* [ ] Update product who we are / categories @joshlambert
* [ ] Update screenshots on category maturity plan slide @joshlambert
* [ ] Update screenshots on telemetry slide @joshlambert
* [ ] Update hiring slide 
  * [ ] PM @joshlambert
  * [ ] Design @jackib
  * [ ] Engineering @cdu1
* [ ] Add/Update throughput slide @cdu1
* [ ] Update priorities/strategy slides @joshlambert
* [ ] Update dogfooding slide @ljlane @fzimmer @JohnMcGuire @joshlambert @awthomas
* [ ] Add/Update recent popular items @joshlambert
* [ ] List recent published content @joshlambert
* [ ] Add topic slides (interesting items that are top of mind that deserve their own slide)
  * [ ] Distribution @ljlane
  * [ ] Geo @nhxnguyen
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @JohnMcGuire
  * [ ] Infrastructure @awthomas
* [ ] Accomplishments & Plans
  * [ ] Distribution @ljlane
  * [ ] Geo @nhxnguyen
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @JohnMcGuire
  * [ ] Infrastructure @awthomas
* [ ] Update Challenges
  * [ ] Distribution @ljlane
  * [ ] Geo @nhxnguyen
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @JohnMcGuire
  * [ ] Infrastructure @awthomas
* [ ] Update Whats Next
  * [ ] Distribution @ljlane
  * [ ] Geo @nhxnguyen
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @JohnMcGuire
  * [ ] Infrastructure @awthomas
* [ ] Final review @joshlambert @cdu1
