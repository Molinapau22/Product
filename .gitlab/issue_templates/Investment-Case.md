# Investment proposal

## Project sponsor

* Use the [Opportunity Canvas](https://about.gitlab.com/handbook/product/product-processes/#opportunity-canvas)

## Project description

TBD

### ROI spreadsheet link

TBD

## ROI calculation spreadsheet

Make a copy of the `ROI calculation spreadsheet` from this [template](https://docs.google.com/spreadsheets/d/1H928LocTb7cWuQoNVwI7kG-z9pwDYmbbQv7Sr141sOc/edit#gid=1846081069) and fill it out.

It includes an analysis of:
* How many of which roles are needed, and when they are needed
* Prediction on increase in ARR
* Prediction on increase in licenses by type
* Prediction on new market TAM impact
* Prediction on reduced support and/or infrastructure costs
* Operational and/or security risk reduction

This is used to compare and contrast this potential investment with others that are under consideration.

### Simplified ROI Sensitivity Analysis

* ROI projections at this early phase are estimates at best. Sensitivity analysis helps showcase the range of possible outcomes to accommodate for the inherent uncertainty with these opportunities. 

| Category | Value |
| --- | --- |
| Low Investent Gain/Loss | $TBD |
| Low ROI | %TBD |
| High Investent Gain/Loss | $TBD |
| High ROI | %TBD |
| Investment Length | TBD years |

#### Forecasted impact on Category 

| Category Scope/Responsibility  | Current Grade  | Grade with Realignment from Existing Teams (+X HC) | Grade With Additional Headcount (+X HC)  |
| --- | --- | --- | --- |
| Add catgeory/ epic link | Add an A-F grade on current team's ability to meet the scope of the results | Add an A-F grade that models impacts of realigning required headcount from existing groups. Include a grade change of the group headcount will be taken from | Add an A-F grade with additional headcount requested in this investment case |
||||

## Tasks

### Opening Tasks
* [ ] Rename this issue adding an appropriate title 
* [ ] Add appropriate Section, Stage and Group labels
* [ ] Create a retrospective thread for this issue template

### Drafting Tasks
* [ ] Complete sections as needed

### Review Tasks
* [ ] Tag relevant stakeholders for awareness

### Closing Tasks
* [ ] Make improvements from the retro thread to this issue template

### Tagged stakeholders

* [ ] Development director TBD
* [ ] UX counterpart TBD
* [ ] SET (quality) counterpart TBD
* [ ] Data team (if applicable) TBD
* [ ] Security team (if applicable)
* [ ] Infrastructure team (if applicable)

## References
* [Product Development Budgeting Process and DRIs](https://about.gitlab.com/handbook/product-development-budgeting/)
* [Other Investment Case Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&state=opened&label_name[]=Investment%20Case)

/confidential

/label ~"Investment Case" 
