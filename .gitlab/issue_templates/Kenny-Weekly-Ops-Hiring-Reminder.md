## Intent
- Don't use this issue for collaboration (it can overload), ping hiring managers in #ops-section or specific hiring channels or in the weekly hiring sync

## References
- **CI**
  - [Recruiter Project](https://www.linkedin.com/talent/hire/487259666/manage/all)
  - [Greenhouse](https://gitlab.greenhouse.io/sdash/4850152002)
- **Generic Posting**
  - [Public Posting](https://boards.greenhouse.io/gitlab/jobs/5259284002)
  - [Greenhouse](https://gitlab.greenhouse.io/sdash/4860751002)
- Cold Email Snippet (already added as template in Recruiter)

## Actions
- [ ] Retro thread
- [ ] Start a thread in #ops-section and make an offer for assistance - @kencjohnston
- [ ] Source 20 new candidates for each of the three roles - @kencjohnston
- [ ] Reach out to 10 top tier candidates via LinkedIn - @kencjohnston
- [ ] Schedule [screening calls](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/issues/724) with general candidates
- [ ] Make [updates](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Kenny-Weekly-Ops-Hiring-Reminder.md)

/assign @kencjohnston

/label ~"KCJ::Doing"

/due Friday
