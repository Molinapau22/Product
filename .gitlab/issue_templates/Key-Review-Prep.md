**Next Product Key Review meeting date: **

**Slide due date:**

**Meeting Agenda:**
https://docs.google.com/document/d/1Hmg2r-VGYUtYqlHDoPCzeJPI350Y58JSGVHy-P33UBc/edit

**Link to Google slides for this month's Key Revew - Product:**

**Link to Google Folder with all Decks:**
https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg

### Opening Tasks: 

* [ ] @jennifergarcia20 - Add meeting dates and slides due date above
* [ ] @jennifergarcia20 - Assign a due date to this issue 
* [ ] @jennifergarcia20 - Add meeting date to fist page of Key Review slides

### Slide Update Tasks

#### Review and update the Business Driver slides
  * [ ]  @justinfarris - make sure projects and owners in slides are update to date; update "up to date" comment on intro slide; tag in team members below for updates as necessary 
  * [ ] @kencjohnston
  * [ ] @david
  * [ ] @joshlambert
  * [ ] @ofernandez2
  * [ ] @hilaqu

#### Review and update Cross-Functional Project Update slides
  * [ ] @justinfarris - make sure projects and owners in slides are up to date; tag in team members below for updates as necessary
  * [ ] @ofernandez2
  * [ ] @keithchung 

#### Review and update OKR Updates slides
  * [ ] @fseifoddini - make sure KRs and owners in slides are in sync with Ally.io; tag in owners of at-risk KRs into the appropriate slide 
  * [ ] @david
  * [ ] @justinfarris
  * [ ] @mushakov 
  * [ ] @keithchung 
  * [ ] @kencjohnston 
  * [ ] @ogolowinski
  * [ ] @jreporter 
  * [ ] @kbychu 
  * [ ] @dorrino 
  * [ ] @hsutor 
  * [ ] @fzimmer

#### Review and update Appendix Metrics
  * [ ] @justinfarris - - make sure projects and owners in slides are update to date; update "up to date" comment on intro slide; tag in team members below for updates as necessary
  * [ ] @fseifoddini
  * [ ] @amandarueda
  * [ ] @hilaqu

#### Review full deck before Key Meeting
  * [ ] @justinfarris
  * [ ] @fseifoddini
  * [ ] @david
  * [ ] @hbenson
  * [ ] @kencjohnston
  * [ ] @joshlambert
  * [ ] @hilaqu
  * [ ] @keithchung
  * [ ] @@ofernandez2

#### Closing Tasks
* [ ] - @justinfarris - For next month, make a copy of this month's Key Review Slides and save them in the [Key Review - Product Folder](https://drive.google.com/drive/folders/1fnOD9MXwfgzscquTPUHQLSTm1xi5-4Bg) 
* [ ] - All - After each Key Meeting [update this Key Review prep issue template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Key-Review-Prep.md) with any added items
* [ ] Anyone - Add a "Retrospective Thread" comment to this issue

/assign @jennifergarcia20 @fseifoddini @hilaqu @david @kencjohnston @joshlambert @justinfarris @keithchung @hbenson @ofernandez2 
/confidential

