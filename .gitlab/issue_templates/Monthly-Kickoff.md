## Issue Tasks
* [ ] Change the title of the issue to include the actual release number for this particular kick-off - @david
* [ ] Add a Retrospective Thread for participating team members to provide feedback in - @david
* [ ] Set the due date for when Kickoff Videos should be completed
* [ ] Please update the keyValue on the Kickoff page to the release we'll be kicking off. You can do this by [opening an MR for this line](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/kickoff/template.html.md.erb#L5). - @david
* [ ] Create a discussion thread on this issue titled `{INSERT} Section Kickoff Highlights` for PMs to add their highlights - Section leaders

## Group Kickoff Meetings (Completed by the 16th of the month)

**Note** - Completing these tasks by the 16th enables the Product Leadership team to have ample time to review videos and planning issues before the 18th. Please make every effort to complete and check-off the below tasks prior to the end of the day on the 15th.

### Preparation
1. Add your planning issues so your manager has an opportunity to review [New ask based on 13.1 retrospective]
1. Schedule a monthly, livestreamed, recorded Kickoff meeting for your group
1. Review the [upcoming releases page](https://about.gitlab.com/upcoming-releases/) and ensure the current list of issues makes sense. If an issue shouldn't appear there - remove the `direction` label
1. Strongly consider creating a release planning issue that links all highlighted issues 
1. Ensure the items for discussion are labeled with the `direction` label and scheduled for the upcoming milestone
1. Update the issues to ensure:
   1. The description is single-source of truth (no digging through comments required)
   1. The description contains a strong Problem Statement, Use Cases and a complete Proposal
   1. The description contains proposed designs
   1. _Note: It is highly recommended to [write your release post item content](https://about.gitlab.com/handbook/marketing/blog/release-posts/#contributing-instructions) at this time, since you've already created it for the kickoff, so you're well ahead of Key dates._
1. Summarize 1-2 highlights for your group on your section specific thread in the discussion of this issue and tag your manager and section leader when complete.
 

### Meeting
1. Consider displaying your stage or group product direction pages during this meeting
1. Try to keep the recording of the kickoff portion of this short. Even if this is scheduled as an agenda item in a regular weekly sync, please record the Kickoff portion of the discussion separately. This way others can review the kickoff playlist for all teams efficiently.
1. Review the `direction` labeled items for the milestone one-by-one, highlighting the problem statement and designs.

### After the Meeting

Once your video is recorded:

1. Post the meeting recording to GitLab Unfiltered, name it `GitLab ##.## Kickoff - Stage:Group` 
1. Assign the video to relevant Youtube playlists.  If the playlist doesn't exist yet, please create one. Include the link to your Direction page, planning issue or issue board reviewed during the video in the video description.  The following list includes the minimally required playlists to add your video to:
   1. Release specific YouTube playlist `##.## Release Kickoff` - will contain all group kickoff videos for a specific release
   1. Your group's specific YouTube playlist `GitLab Group Kickoffs - STAGE:GROUP` - will contain all of your group's specific kickoff videos
1. Add the link to the Planning issue (if appropriate) in the video description
1. Post the link to the recording in your section, stage (#s_) and group (#g_) slack channels
1. Update this issues description to include links to the video and planning issue below 
1. Add links to your "planning issue" and direct video link

#### Tasks

* [ ] Manage:Authentication and Authorization - @hsutor - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp4Sv3eCZbZKtmMox3NipN-) - video - planning issue
* [ ] Manage:Workspace - @ogolowinski - [group playlist]() - video - planning issue
* [ ] Manage:Compliance - @sam.white - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq48JVbIHqWFJ0IxQeIAtEM) - video - planning issue
* [ ] Manage:Import - @ogolowinski - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqqfLsMNj8EhZ9FoG1-54vH) - video - planning issue
* [ ] Manage:Optimize - @hsnir1  - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpoUh9zJ7qdUNwQunTgqLxk) - video - planning issue
* [ ] Plan:Project Management - @gweaver - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoVOLOpXpnvmWwvUCieXJ-K) - video - planning issue
* [ ] Plan:Product Planning - @mushakov - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrIU9UtPgDDc7M3EE3GRNfV) - video - planning issue
* [ ] Plan:Certify - @mjwood - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpRGDNj5qwy5YOj9eEQ6Ut_) - video - planning issue
* [ ] Create:Source Code - @tlinz - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqLWXT14d0V1OWRMDPfmdxU) - video - planning issue
* [ ] Create:Code Review - @phikai - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrkpA5TRbTcGLSWSxSXSrKG) - video - planning issue
* [ ] Create:Editor - @ericschurter - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpgl_ChXIzlTZclv4379xM1) - video - planning issue
* [ ] Create:Gitaly - @mjwood - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8vcQJc4-6J_g7ohpsQIkz) - video - planning issue
* [ ] Ecosystem: Integrations - @g.hickman - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqEe3lE8vMpdvDr5qmJTWTM) - video - planning issue
* [ ] Ecosystem: Foundations - @cdybenko - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrzdBlAcQT7_I52xFFFgBAl) - video - planning issue
* [ ] Verify:Pipeline Execution - @jheimbuck_gl - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrGKYECOTyRT1R0Ji-kYFmz) - video - planning issue
* [ ] Verify:Pipeline Authoring - @dhershkovitch - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrGKYECOTyRT1R0Ji-kYFmz) - video - planning issue
* [ ] Verify:Runner - @DarrenEastman - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrcC7xS0y8a8S9PlElg48bZ) - video - planning issue
* [ ] Verify:Pipeline Insights - @jreporter - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KradvCrf8fL2pggEn5D3w2k) - video - planning issue
* [ ] Package:Package - @trizzi - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoVP8cJft-Hv4kQH__aFaGS) - video - planning issue
* [ ] Release:Release - @cbalane - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kqj3Dk9qD5CIrpQdLbaxrXf) - video - planning issue
* [ ] Configure:Configure - @nagyv-gitlab - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KoqJ_uOHiII_E46XzdK-eB7) - video - [planning issue](https://gitlab.com/gitlab-org/configure/general/-/issues?label_name[]=Planning%20Issue)
* [ ] Monitor:Respond - @abellucci - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrUe3MhOsFQGSv2uoN_Ca_B) - video - planning issue
* [ ] Monitor:Observability - @sebastienpahl - [group playlist TBD]() - video - planning issue
* [ ] ModelOps - @tmccaslin - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpeYKJ_UmMkc63cjynpsq4o) - video - planning issue
* [ ] Secure:Static Analysis - @connorgilbert - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpG-Vf4OE_1ZcGCfl4hqoJ1) - video - planning issue
* [ ] Secure:Dynamic Analysis - @derekferguson - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqHxaetI2k4hZrrStTcTtCQ) - video - planning issue
* [ ] Secure:Composition Analysis - @NicoleSchwartz - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqxyGS_Pzc8jqs4BjSzHkk4) - video - planning issue
* [ ] Secure:Threat Insights - @matt_wilson - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqdzV37vP42lXMOjbh67J8o) - video - planning issue
* [ ] Secure:Vulnerability Research - @connorgilbert - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpoZluYjUAJs3bb1-86cca_) - video - planning issue
* [ ] Protect:Container Security - @sam.white - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqRVEkGsQF-EkkToGjyKLYv) - video - planning issue
* [ ] Enablement:Distribution - @dorrino - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr_mLueUMhLJ7-Mc7f9eCYc) - video - planning issue
* [ ] Enablement:Geo - @nhxnguyen - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KofRx080zwe0VxUEv5yG_nO) - video - planning issue
* [ ] Enablement:Memory - @iroussos - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq1HDOIfQ8ov6lfyJkWK2Yr) - video - planning issue
* [ ] Enablement:Search - @JohnMcGuire - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqBMod_imaJaWeDPb7YdS6l) - video - planning issue
* [ ] Enablement:Database - @iroussos - [group playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqP3MYrcoQHrqPUqn_jJZSN) - video - planning issue
* [ ] Enablement:Sharding - @fzimmer - [group playlist]() - video - planning issue
* [ ] Fulfillment: - @justinfarris - group playlist - video -planning issue


## Company Kickoff Meeting (Completed by the 18th of the month)

### Preparation

1. The VP of Product will be the [directly responsible individual](/handbook/people-group/directly-responsible-individuals/) for presenting and leading the meeting.
1. The format of the meeting will be a relay -starting off with the introducton and then rolling through each section in the alloted time.
2. The sequence of sections and the time per section is decided on prior to the call. VP of Product seeks inputs from the team and decides on the sequence. 
1. If a Director of Product Management is out of office on the day of the kickoff
   call, they should arrange for another Director of Product Management, or
   their delegate, to cover that section and inform the VP of Product ahead of
   time.
1. Ensure that the kickoff page is pulling issues for the upcoming milestone by updating `<% keyValue = "<milestone>"` in `source/direction/kickoff/template.html.md.erb` 

### Meeting

1. Follow the same instructions for [live streaming a Group Conversation](/handbook/people-group/group-conversations/#livestreaming-the-call) in order to livestream to GitLab Unfiltered. People Ops Specialists can help set up the Zoom webinar if not already attached to the Kickoff calendar invite. 
1. The person presenting their screen should make sure they are sharing a smaller window (default YouTube resolution is 320p, so don't fill your screen on a 1080p monitor. 1/4 of the screen is about right to make things readable.)
1. The VP of Product starts the meeting by:
   * Giving a small introduction about the topic of this meeting
   * Introducing the panel interviewees and themselves
   * Reminding anyone who may be watching the video/stream about how we [plan ambitiously](#ambitious-planning).
1. During the discussion about a product section
   * Each panel interviewee screen shares on their own so they can drive according to their rhythm. WE can revisit this if interviewees end up taking more time than allotted.
   * Interviewee should also be sure to use display cues (highlighting, mouse pointer movement) to indicate where in the document we are, so nobody watching gets lost.
   * The interviewee will explain the problem and proposal of listed items. If there is a UX design or mockup available, it will be shown.
   * Each Director of Product should try to have one visual item that can be opened up and looked at.
   * Be sure to mention the individual PMs for the stages and groups within your section while reviewing the issues they've highlighted.
   * Be sure you're on do not disturb mode so audio alerts do not play.
1. The VP of Product often ends the meeting by quickly highlighting several high impact issues and communicating our excitement for the
   upcoming release. Consider even using one of our popular phrases: “This will be the best release ever!”

### Tasks

* Section Leader Prep - check when you have:
   1. Reviewed issue titles and descriptions for your section
   1. Reviewed your section's group kickoffs videos
   1. Prepared your notes for the live Kickoff call. Consider [using this shared notes doc](https://docs.google.com/document/d/1Bk7r2zO25OsZgg9JayVlYoVxKejrFOlkWmY7XM1h5JM/edit#heading=h.q2ylfmejjoxp). 
  * [ ] Dev Section - @ogolowinski
  * [ ] Sec Section - @hbenson
  * [ ] Ops Section - @kencjohnston
  * [ ] Enablement Section - @joshlambert
* Pre meeting
  * [ ] Ops Section - Add a highlights comment based on your review to this issue and share in Slack - @kencjohnston
  * [ ] Ops Section - Update Link in [Ops Section Exciting Things](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/sites/handbook/source/handbook/product/categories/ops/index.html.md.erb) to the highlights comment - @kencjohnston
* Post meeting
  * [ ] Add the Release Kickoff Video Link to the [kickoff page](https://about.gitlab.com/direction/kickoff/) (2nd link in 1st paragraph under 'Overview') - @kristie.thomas (or whoever is streaming the meeting)
  * [ ] Ensure that the playlist link is updated on the [kickoff page](https://about.gitlab.com/direction/kickoff/) (3rd parahraph under 'Overview') - @kristie.thomas (or whoever is streaming the meeting)
  * [ ] Add the livestreamed recording to the `XX.XX Release Kickoff` YouTube playlist and name it `XX.XX Monthly Release Kickoff (Public Livestream)` - @kristie.thomas (or whoever is streaming the meeting)
  * [ ] Move the Release Kickoff recording to the top of the XX.XX Release Kickoff Playlist - @kristie.thomas (or whoever is streaming the meeting)
  * [ ] Trim the early few minutes of silence in the kick-off meeting as soon as the livestream is complete - @kristie.thomas (or whoever is streaming the meeting)
  * [ ] Mention the availability of the YouTube playlist in #whats-happening-at-gitlab slack channel - @david
  * [ ] Before closing this issue ensure Retrospective Thread items have been addressed (preferably via an MR [to this template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Kickoff.md) or [other automated issue config](https://gitlab.com/gitlab-com/Product/#gitlab-product-process)) - @@david


/assign @kencjohnston @david @joshlambert @kristie.thomas @jreporter @kbychu @mushakov @hbenson @ogolowinski @sarahwaldner @fzimmer @jennifergarcia20
