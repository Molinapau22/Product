## :dart: Intent
The intent of this issue is to review and prioritize any Ops Section Investment Cases.

## :book: References
- [Current +10 Investment Opportunities]() 
- [Proposed Investment Case Board](https://gitlab.com/gitlab-com/Product/-/boards/2427050?&label_name%5B%5D=Investment%20Case&label_name%5B%5D=section%3A%3Aops) 

## Tasks

### :wave: Open
* [ ] Create retrospective Thread
* [ ] Ask PM Leaders (Jackie, Tim, Kevin, Seb) if there are any pending Investment Cases to document
* [ ] Set Due Date for when review will begin
* [ ] Put time on your calendar for the review

### :pencil2: Review Actions
Actions for reviewing and prioritizing
* [ ] Review and Prioritize Investment Cases on the board - @kencjohnston
* [ ] [Update the +10 Handbook Content](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/source/direction/ops/template.html.md.erb#L280) - @kencjohnston
* [ ] Note any updates in Slack after review and merge - @kencjohnston

### :new: Update Review and Evaluation Process
* [ ] [Make updates to the handbook page](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/sites/handbook/source/handbook/product/categories/ops/index.html.md.erb#L59) that documents the review and evaluation process.

### :x: Close
* [ ] Record a video summary of our current and historical investment. Share in Slack and add to the Direction page.
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Monthly-Ops-Investment-Case-Review.md) from the Retro Thread

/assign @kencjohnston
/due in 23 days
/label ~"section::ops"
