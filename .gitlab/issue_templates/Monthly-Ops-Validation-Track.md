## :dart: Intent
The intent of this issue is to get a view of current Validation Track activities in order to:
- Share insights learned across a Product groups
- Highlight instances where groups might be working on similar validation track activities
- Provide coaching to PMs on validation track activities

## :book: References
- [Current Validation Track Initiatives](https://about.gitlab.com/direction/ops/#current-validation-track-initiatives) 
- [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Validation%20Track%20Review) 

## Tasks

### :wave: Open
* [ ] Create retrospective Thread
* [ ] Assign PM Leaders (Jackie, Kevin) tasks and assign issue to them

### :exclamation: Extras
* [ ] If first week of the quarter - Review previous quarter issues for trends, consider other sources

### :pencil2: Review Actions
Actions for issues in your assigned type
- Review the recent issues referenced in Validation Track activity links above
- Look for insights that would be useful to a broader audience, including where two groups are working on a similar topic
- Highlight relevant discovers across the #ops-section and #product channels

### :heavy_check_mark: Review Complete
- [ ] [Ops Section Solution Validation](https://gitlab.com/groups/gitlab-org/-/issues?sort=updated_desc&scope=all&state=all&label_name[]=section%3A%3Aops&label_name[]=workflow%3A%3Asolution%20validation) - @
- [ ] [Ops Section Problem Validation](https://gitlab.com/groups/gitlab-org/-/issues?sort=updated_desc&scope=all&state=all&label_name[]=section%3A%3Aops&label_name[]=workflow%3A%3Aproblem%20validation) - @
- [ ] [NOT Ops Section Problem Validation](https://gitlab.com/groups/gitlab-org/-/issues?sort=updated_desc&state=all&label_name[]=workflow::problem+validation&not[label_name][]=section::ops) - @

### :x: Close
* [ ] Post a Highlights Thread and share it in the Ops Section and Product channels, post highlight to Ops Section Highlights page - @kencjohnston
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Monthly-Ops-Validation-Track.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" ~"Validation Track Review"

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338

/due Thursday
