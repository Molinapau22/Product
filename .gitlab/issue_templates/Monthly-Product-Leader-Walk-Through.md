## :dart: Intent

Ensure our product remains cohesive and the user experience remains great by reminding the product leadership team to review cross-product experiences and experiences from parts of the product that they are less familiar and sharing those with the appropriate Product Groups.

## :book: References
* [Walk Through Handbook Page](https://about.gitlab.com/handbook/product/product-processes/#walk-through)
* [Walk Through Issue Template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Product-Walk-Through.md)
* [Walk Through Issue Backlog](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20walkthrough)
* [This issue template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Monthly-Product-Leader-Walk-Through.md)

## Tasks

### :wave: Open
* [ ] Create Retrospective Thread - @kencjohnston

### Product Leaders
Each month, each leader should:
1. Review [Backlog of Walk Throughs](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=product%20walkthrough)
1. Select and prepare for a walk through from outside of your section, schedule the issue
1. Consider adding new Walk Through suggestions from your or other areas by submitting an issue [using the template](https://gitlab.com/gitlab-com/Product/issues/new?issuable_template=Product-Walk-Through)

When you've completed your monthly walk-through issue reference it here and check the box below.
* [ ] Product Management: @adawar
* [ ] Dev: @david
* [ ] Ops: @kencjohnston 
* [ ] Sec: @hbenson
* [ ] Growth: @hilaq
* [ ] Enablement: @joshlambert 

## Closing
* [ ] Summarize Highlights in a comment and share broadly (#product and #whats-happening channels, `@gl-product` memntion) - @kencjohnston
* [ ] Make [updates to this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Monthly-Product-Leader-Walk-Through.md) based on retrospective thread - @kencjohnston

FYI @sfwgitlab

/assign @david @kencjohnston @joshlambert @hbenson @hilaqu

/due in 28 days

/label ~"product walkthrough" 
