Please add your feedback by **2022-MM-DD**. Thank you!

In Q#, we'll continue with Shared R&D OKRs and leveraging Ally.io as SSOT.

Feedback on this iteration of the OKR process can be shared in this issue. 

***

Start your comment with the appropriate icon on the front of it to indicate which category it falls under. Create individual comments for every entry so that each item can have its own thread. 

- :thumbsup: What went well?
- :thumbsdown: What didn't go well?
- :bulb: What can we improve?
- :question: Items that are relevant for discussion but not a clear good or bad event to improve upon

/label ~"Product Operations"
/label ~"OKR"
/label ~"workflow::In review"
/assign @fseifoddini @brhea
/confidential