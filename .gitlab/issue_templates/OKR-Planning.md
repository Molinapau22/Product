# Overview

This issue will be used to draft the Shared R&D OKRs and any Product Team specific OKRs for FYYY-Q#. Product and Engineering share one issue as these OKRs often are related and require input from cross-functional team leaders as well as contributions across Product Groups.

These specific Objectives and Key Results roll up to the Company Objectives and are approved by the CProdO, CTO, and CEO.  The CProdO will drive collecting feedback in this issue. Once finalized and approved by the CEO, Shared R&D OKRs will be moved to Ally.io as SSOT by Product Operations and this issue will be closed.

## Initial todos

This issue is auto-generated and some sections and links will need to be updated to reflect the current period.

- [ ] Update all FYYY-Q# references in this issue with the current FY and Q values.
- [ ] Find and replace the `TODO_ADD_LINK` values in this issue
- [ ] Optional: Delete this section when initial todos are complete

## ProdOps todos: @fseifoddini

- [ ] Update this issue and the [product OKR page](https://about.gitlab.com/handbook/product/product-okrs) for the upcoming quarter 
- [ ] `@mention` the EBA to CTO and CProdO in a comment as an FYI
- [ ] Add links to Shared R&D OKR agenda [such as](https://about.gitlab.com/handbook/product/product-okrs/#okr-kick-off-by-the-product-and-engineering-leadership-teams-1)
- [ ] Assign this issue to CProdO
- [ ] `@mention` CProdO in a comment to initiate OKR collaboration

## Related Links

- [Engineering Draft OKRs](TODO_ADD_LINK)
- [Product OKR process](https://about.gitlab.com/handbook/product/product-okrs/#FYYY-Q#-product-okr-process).
- [FYYY-Q# feedback issue](TODO_ADD_LINK)

### Shared R&D Objective 1: PLACEHOLDER

Owners: `@mention`

**Key results:**

1. PLACEHOLDER
1. PLACEHOLDER
1. PLACEHOLDER
1. PLACEHOLDER

### Shared R&D Objective 2: PLACEHOLDER

Owners: `@mention`

**Key results:**

1. PLACEHOLDER
1. PLACEHOLDER
1. PLACEHOLDER
1. PLACEHOLDER

### Shared R&D Objective 3: PLACEHOLDER

Owners: `@mention`

**Key results:**

1. PLACEHOLDER
1. PLACEHOLDER
1. PLACEHOLDER
1. PLACEHOLDER

---------

- [Ally.io link to upcoming quarter Engineering OKRs](TODO_ADD_LINK)
- [Ally.io link to upcoming quarter Product OKRs](TODO_ADD_LINK)
  
---------

/label ~"Product Operations"
/label ~"workflow::In dev"
/assign @fseifoddini