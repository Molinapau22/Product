## Proposal

The previous quarter is over, so it is time to draw our conclusions, share the learnings and celebrate our successes. This issue invites everyone for a retrospective on their OKRs in the past quarter. 

Participants have 2 weeks to participate. After the retro period is over, the retrospective owner will send a summary to Slack #product or PM weekly if appropriate.

## How to proceed

If you did not set OKRs, then this is a great opportunity to learn from the others so it's recommended you still follow and review this issue.

If you did set OKRs, then please, start a new thread using the following template to retro your OKRs and read + provide feedback on the other retro threads.

### Template

```
## <Section/Stage/Group name>

- list here the OKRs and their final status in percentage (links to the KR in Ally.io or any related GitLab issues recommended)

---

The following questions are here to support you discussing the results:

- We aim to have an average KR score between 70-85%. If your results are below 70%, think about what you can improve to deliver next time. If your results are above 85%, can you be more ambitious next time?
- Did you or your EM counterpart report regularly about the OKR status to your team? Were OKRs ever discussed with the team?
- Were your key results binary or continuous metrics? Do you have ideas in retrospect for continuous key results?
- What parts of the process did you find the most difficult (if any)?
```

/label ~"Product Retrospectives" ~OKR 
/due in 2 weeks
