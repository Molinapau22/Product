## Intent
To increase awareness of our SaaS Performance indicators for product groups with particularly high usage across the section and company. To provide a regular, timely review of those performance indicators to spot trends and mobilize response quickly.

https://about.gitlab.com/handbook/product/categories/ops/#saas-reviews

## References
- [PA Error Budget](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#verifypipeline-authoring---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-pipeline_authoring/stage-groups-group-dashboard-verify-pipeline-authoring?orgId=1)
- [PE Error Budget](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#verifypipeline-execution---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-pipeline_execution/stage-groups-group-dashboard-verify-pipeline-execution?orgId=1&from=now-28d&to=now)
- [Runner Error Budget](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#verify-verifyrunner---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-runner/stage-groups-group-dashboard-verify-runner)
- [Runner SLO Attainment](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#verifyrunner---other---ci-runners-slo-trends) - [Grafana Dashboard](https://dashboards.gitlab.com/d/general-slas/general-slas?from=1609459200000&orgId=1&to=now&viewPanel=23)
- [Package Error Budget](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#packagepackage---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-group-dashboard-package-package)
- [Pipeline Insights Error Budget](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#verify-verifytesting---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-pipeline_insights/stage-groups-pipeline-insights-group-dashboard?orgId=1)
- [Release Error Budget](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#releaserelease---error-budget-for-gitlabcom) - [Grafana Dashboard](https://dashboards.gitlab.net/d/stage-groups-release/stage-groups-group-dashboard-release-release?orgId=1)

## Review Format
Provide updates in the [ops_section.yaml](https://gitlab.com/internal-handbook/handbook/-/blob/master/data/performance_indicators/ops_section.yml) in the internal handbook for your preformance indicators inline with our standard performance indicator review (update health and add `Insights` and `Improvements`).

Note - at the moment approval rules are required to merge. We are working to fix this.

## Tasks

### Opening Tasks
- [ ] Retro Thread - @kencjohnston
- [ ] At the start of every quarter ask PMs if they'd like to participate - @kencjohnston

### Review Tasks
- [ ] Verify:PE Review Complete and Shared in Slack - @jheimbuck_gl
- [ ] Verify:Runner Review Complete and Shared in Slack- @darreneastman
- [ ] Verify:Pipeline Insights Review Complete and Shared in Slack - @jreporter
- [ ] Package:Package Review Complete and Shared in Slack - @trizzi
- [ ] Release:Release Review Complete and Shared in Slack - @cbalane
- [ ] Verify:PA Review Complete and Shared in Slack - @dhershkovitch

### Closing Tasks
- [ ] Post Highlights - @kencjohnston
- [ ] Post Highlights in #ops_section Slack
- [ ] [Updates](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Async-SaaS-Review.md) from the Retro Thread


/assign @jreporter @DarrenEastman @trizzi @sgoldstein @kencjohnston @dcroft @jheimbuck_gl @cbalane @dhershkovitch

/due Sunday
