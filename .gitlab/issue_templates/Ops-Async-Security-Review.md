## Intent
To increase awareness of our Security performance indicators and issues for product groups within the Ops Section. 

## Review Document
- [Ops Section Security Dashboard](https://app.periscopedata.com/app/gitlab/997755/Ops-Section-Security-Issue-Dashboard)
- [Live Issue List](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_asc&state=opened&label_name%5B%5D=vulnerability&label_name%5B%5D=section::ops&not%5Blabel_name%5D%5B%5D=severity::4)

## Additional References
* [Past Due Security Issues Dashboard](https://app.periscopedata.com/app/gitlab/913607/Past-Due-Security-Issues-Development) 
* [Development Department KPI](https://about.gitlab.com/handbook/engineering/development/performance-indicators/#past-due-security-issues)
* [Engineering Metrics Dashboards](https://about.gitlab.com/handbook/engineering/metrics/) (Past due metrics by group)
    * [Ops Section](https://about.gitlab.com/handbook/engineering/metrics/ops/)
        * [Verify](https://about.gitlab.com/handbook/engineering/metrics/ops/verify/)
        * [Package](https://about.gitlab.com/handbook/engineering/metrics/ops/package/)
        * [Release](https://about.gitlab.com/handbook/engineering/metrics/ops/release/)
        * [Configure](https://about.gitlab.com/handbook/engineering/metrics/ops/configure/)
        * [Monitor](https://about.gitlab.com/handbook/engineering/metrics/ops/monitor/)
* [Past Review Issues](https://gitlab.com/gitlab-com/Product/-/issues?sort=created_date&state=all&label_name[]=Weekly+Security+Review)

## Review Format
Ops Section Leaders review open and past-due Security issues by Severity to ensure they are receiving the appropriate Global prioritization. 
- Identify past-due security issues and inquire about making them smaller to ship earlier
- Identify new security issues and ensure they are picked up early to prevent SLO violations

## Tasks

### Opening Tasks
- [ ] Retro Thread - @kencjohnston
- [ ] Consider additional references/resources - @kencjohnston
- [ ] If first week of the month ping assignees in new thread to identify any trends - @kencjohnston

### Review Tasks
- [ ] Verify - Cheryl reviews and FYIs Jackie on all comments
- [ ] Package - Dan reviews and FYIs Kenny on all comments
- [ ] Release - Dan reviews and FYIs Kevin on all comments
- [ ] Configure - Dan reviews and FYIs Kevin on all comments
- [ ] Monitor - Sam reviews and FYIs Kevin on all comments

### Closing Tasks
- [ ] If first week of the month review trends and add to summary - @kencjohnston
- [ ] Post Summary Comment - @kencjohnston @sgoldstein
- [ ] Post Summary Comment link in #ops_section Slack - @kencjohnston @sgoldstein
- [ ] [Updates](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Async-Security-Review.md) from the Retro Thread

### On Going Dashboard Improvement Requests
- [ ] Add an overdue flag to the table
- [ ] Add a milestone column to the table
- [ ] Add a workflow status label to the table

/assign @sgoldstein @dcroft @cheryl.li @kencjohnston @kbychu @jreporter @dcouture @vdesousa 

/label ~"Weekly Security Review" ~"section::ops"

/due Wednesday
