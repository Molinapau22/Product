## Intent
Quarterly [Ops Section Direction](https://about.gitlab.com/direction/ops/) Review and AMA

## Everyone Can Contribute
- Please see the [GoogleDoc for contribution instructions](https://docs.google.com/document/d/18gidNGDDbDYxqwv1SsdlbJdNxrTToZjFfQfmISaztxQ/edit#heading=h.5b5tqh951i64), you can contribute synchronously or synchronously.

## Tasks
- [ ] Update contribution method in doc
- [ ] Schedule AMA/Review
- [ ] Communicate to team
- [ ] Followup tasks
- [ ] Followup on retro thread