## :dart: Intent
This issue is meant to provide a reminder and a place to collaborate on a [review of MVC, New Config and Pricing/Tiering issues](https://about.gitlab.com/handbook/product/product-management/process/#mvc-and-new-config-reviews) scheduled in the upcoming milestone (the one you are about to Kickoff) between Product Managers and Product Leaders in the Ops Section.

This process is entirely optional - and is intended to provide a reminder that some tough product decisions benefit from an outside review. Feel free to use your peers, manager or other leaders to help challenge your product decisions.

## Opening Tasks

* [ ] Create Retrospective Issue thread - @kencjohnston
* [ ] Set issue due date (four working days before the 18th) - @kencjohnston 
* [ ] Pings PMs and ask them to add planning issue links - @kencjohnston
* [ ] Ping GMPs and PMs and encourage them to review each other's planning issues - @kencjohnston

## Planning Issues 
* [ ] Verify:Pipeline Execution - @jheimbuck_gl - Issue Link+
* [ ] Verify:Pipeline Authoring - @dhershkovitch - Issue Link+
* [ ] Verify:Runner - @DarrenEastman - Issue Link+
* [ ] Verify:Pipeline Insights - @jreporter - Issue Link+
* [ ] Package:Package - @trizzi - Issue Link+
* [ ] Release:Release - @cbalane - Issue Link+
* [ ] Configure:Configure - @nagyv-gitlab - Issue Link+
* [ ] Monitor:Respond - @abellucci - Issue Link+
* [ ] Monitor:Observability - @sebastienpahl - Issue Link+

## (Optional) Planning Issue Review
The [intent](#introduction) of this issue is to provide a reminder to bring a second set of eyes to certain types of issues planned for the upcoming release. As an alternative to the specific task pings below - you could request your manager review your entire Planning Issue.

* [ ] Verify:Pipeline Execution - @jheimbuck_gl - Ping Product Leader Planning Issue
* [ ] Verify:Pipeline Authoring - @dhershkovitch - Ping Product Leader Planning Issue
* [ ] Verify:Runner - @DarrenEastman - Ping Product Leader Planning Issue
* [ ] Verify:Pipeline Insights - @jreporter - Ping Product Leader Planning Issue
* [ ] Package:Package - @trizzi - Ping Product Leader Planning Issue
* [ ] Release:Release - @cbalane - Ping Product Leader Planning Issue
* [ ] Configure:Configure - @nagyv-gitlab - Ping Product Leader Planning Issue
* [ ] Monitor:Respond - @abellucci - Ping Product Leader Planning Issue
* [ ] Monitor:Observability - @sebastienpahl - Ping Product Leader Planning Issue

## (Optional) MVC, Tiering, and New Config Review Tasks
Please `@mention` your Product Leader on any relevant issues:
- MVC issues to help challenge breakdown and [iteration](https://about.gitlab.com/handbook/product/product-principles/#iteration)
- New Config issues to challenge whether new configuration (instead of [convention](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration)) is needed
- Pricing/Tiering issues to help challenge your designation of appropriate tier based on our [Buyer Based Open Core](https://about.gitlab.com/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier) model

* [ ] Verify:Pipeline Execution - @jheimbuck_gl - Ping Product Leader on Issues
* [ ] Verify:Pipeline Authoring - @dhershkovitch - Ping Product Leader on Issues
* [ ] Verify:Runner - @DarrenEastman - Ping Product Leader on Issues
* [ ] Verify:Pipeline Insights - @jreporter - Ping Product Leader on Issues
* [ ] Package:Package - @trizzi - Ping Product Leader on Issues
* [ ] Release:Release - @cbalane - Ping Product Leader on Issues
* [ ] Configure:Configure - @nagyv-gitlab - Ping Product Leader on Issues
* [ ] Monitor:Respond - @abellucci - Ping Product Leader Planning Issue
* [ ] Monitor:Observability - @sebastienpahl - Ping Product Leader Planning Issue

## Closing Tasks
* [ ] Review Retrospective thread and [update template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Section-MVC-Config-Pricing-Review.md) before closing - @kencjohnston

/assign @kencjohnston @jreporter @kbychu @cbalane @dhershkovitch @DarrenEastman @jheimbuck_gl @trizzi @nagyv-gitlab @abellucci @sebastienpahl 

/label ~"MVC & Config Review" ~Doing
