## :book: References

* [Handbook Page](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/)
* [PI YAML Data Fields](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#data)
* [PI Implementation Status Reference](https://about.gitlab.com/direction/product-analytics/#implementing-product-metrics)
* [Meeting Notes](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#)
* [Past Prep Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Aops)


## :dart: Intent

* [Ops Section PIs - Intent](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#intent)

## :white_check_mark: Tasks

### :o: Opening Tasks
* [ ] Set a due date to this issue as 2 business days prior to the scheduled review to make time for Leadership async review - @kencjohnston
* [ ] Add a retrospective thread to this issue - @kencjohnston
* [ ] Update the [Meeting Notes](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) with the timing and this Prep Issue
* [ ] Coordinate timing of reviews from Group Managers
* [ ] Coordinate with Jen Garcia on this issue regarding when the content will be ready for David's review
* [ ] Put a block on your calendar for Section Leader Review - @kencjohnston
* [ ] Review the [update actions](#star-performance-indicator-yaml-updates) and ensure they are accurate per the [Data definitions](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#data) - @kencjohnston

### :clock3: Quarterly Tasks
* [ ] If the first month of the quarter - Start a new discussion thread with PM team about how they set and attained their previous quarter results/PI targets/OKRs - @kencjohnston
* [ ] If the first month of the quarter - Add a reminder to include both their previous quarter and next quarter targets and discuss quarterly attainment in their insights/health as well as progress to annual targets - @kencjohnston
 
### :heavy_check_mark: Other Tasks
* [ ] Remind team about the shift to monthly-focus - @kencjohnston
* [ ] Remind team to add `monthly_estimated_targets` to SMAU targets (do that for Ops CMAU as an example) - @kencjohnston
* [ ] Remind team - add Tertiary Charts to SMAU AND CMAU with projections - @kencjohnston
* [ ] Ensure we https://gitlab.com/gitlab-com/Product/-/issues/3978+ - @kencjohnston
* [ ] Check remaining follow ups from [last review](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Aops) - @kencjohnston

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Ops Section Performance Indicators YAML file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/master/data/performance_indicators/ops_section.yml) inline with the [Data properties](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#data) for all PIs each month, including:

* Update your `health:` both `level:` and `reason:` with your progress and likelihood of reaching your `target:`
* Review your `definition:` to include both what specifically is being measured and the **rationale** for why you chose that measurement
* Review your `target:` to ensure it is updated, appropriate, ambitious and highlights why you have the expected growth rate, particularly in relation to GitLab's [UMAU growth rate](https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau). For SMAU and CMAU this should include quarterly and annual targets. 
* Update your `health:` level and `implementation:` status
* Update any `funnel:` to reference stage/group direction or handbook page for the growth model and funnel
* Update any `urls:` to reference additional Sisense dashboards for more details
* Update `lessons:` `learned:` with `Insight -` prefixed comments about insights gained from the last month of data
* Update `monthly_focus:` Comments about planned improvements to reach your goals over the next two milestones 
* If there have been recent updates to the definition, rationale, growth model, funnel highlight those and include those as `reasons:` under `health:`
* If you don't have a defined metric, rationale, growth model or funnel in the handbook reference issues for creating those and provide status on blockers in the `reasons:` under `implementation:`.

:heavy_check_mark: - It is important to ensure your updates are always evergreen. If you have no updates for the current month state as much in the appropriate fields and remove updates from the previous month.

_Note_ - These instructions are based off of currenting understanding of the values defined in the [Data section](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#data) for Performance Indicator pages. They may change.

#### Group Updates

* [ ] Update Section Wide - @kencjohnston - MR URL+
* [ ] Update Verify SMAU Funnel - @jreporter - MR URL+
* [ ] Update Verify:Pipeline Execution - @jheimbuck_gl - MR URL+
* [ ] Update Verify:Pipeline Authoring - @dhershkovitch - MR URL+
* [ ] Update Verify:Runner - @DarrenEastman - MR URL+
* [ ] Update Verify:Pipeline Insights - @jreporter - MR URL+
* [ ] Update Package - @trizzi - MR URL+
* [ ] Update Release - @cbalane - MR URL+
* [ ] Update Configure - @nagyv-gitlab - MR URL+
* [ ] Update Monitor:Respond - @abellucci - MR URL+
* [ ] Update Monitor:Observability - @sebastienpahl - MR URL+

### :package: Pre-Meeting Tasks
* [ ] Consider pre-recording an overview - @kencjohnston
* [ ] Ensure ample time for Group Manager review of all stages - @kencjohnston
   - [ ] @kbychu review
   - [ ] @jreporter review
* [ ] Ask questions in the [doc](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) - @kencjohnston
* [ ] Pay special attention to progress towards annual goals for CMAU and SMAU - @kencjohnston
* [ ] Ping David to confirm content is ready for review in the [doc](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) 
* [ ] Share the upated PI handbook page in the #ops-section and #product Slack channels 2 business days in advance of the review and encourage async participation - @kencjohnston

### :night_with_stars: Post-Meeting Tasks
* [ ] Capture actions discussed in the review in the [follow-up actions](#follow-up-actions) section

### :x: Closing Tasks
* [ ] Add a reminder to team members to capture their followups in issues and add them to the description - @kencjohnston
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Section-PI-Review.md) based on the retrospective thread - @kencjohnston
* [ ] Add a highlights comment and share in Slack - @kencjohnston
* [ ] Update the link in [Exciting Things](https://gitlab.com/gitlab-com/www-gitlab-com/-/edit/master/sites/handbook/source/handbook/product/categories/ops/index.html.md.erb#L177) to the new highlights comment - @kencjohnston
* [ ] Based on any adjustments to PI targets from individual stages and groups - adjust overall CMAU targets ([example](https://gitlab.com/gitlab-com/Product/-/issues/3443))

### Follow Up Actions
* [ ] ACTION @


/assign @kencjohnston @kbychu @dhershkovitch @DarrenEastman @jheimbuck_gl @trizzi @jreporter @nagyv-gitlab @abellucci @cbalane @sebastienpahl


/label ~"section::ops"  ~"PI Review Prep"
