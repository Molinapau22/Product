## Intent
This issue describes how broader group and team members are [facilitating a Product Manager to take meaningful time away from work](https://about.gitlab.com/handbook/product/product-manager-role/#taking-time-off). The intent is that the PM isn't required to "catchup" before and after taking a well-deserved vacation. 

## :handshake: Responsibilities
Include creating release post items, weekly triage issues, responding to community or customer requests, preparing a Kickoff video, prioritizing issues, responding to clarification in current milestone work, etc.

| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |
| HIGH/MEDIUM/LOW | Respond to any customer requests | [Direction Page]() | Your Manager | Your Designer/EM | 

## :muscle: Coverage Tasks
Include specific issue links for tasks to be completed. Solution validation issues to progress, kickoff, PI or GC prep, etc.
- [ ] Item - Link - `@assignee`

## :book: References
Here are some references for how I and my group generally works:
- [Group Process Handbook Page](ADD LINK)
- [Key direction pages for triaging](ADD LINK)
- [Additional References](ADD LINK)

## :white_check_mark: Issue Tasks

### :o: Opening Tasks
- [ ] Assign to yourself
- [ ] Title the issue `PM Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
- [ ] Add an issue comment for your :recycle: Retrospective Thread
- [ ] Assign due date for your last PTO day
- [ ] Add any relevant references including direction pages, group handbook pages, etc
- [ ] Fill in the Responsibilities table with broad based responsibilities
- [ ] Fill in the specific Coverage Tasks with distinct items to complete and assignees
- [ ] Assign to anyone with a specific task or responsibility assigned to them
- [ ] Share this issue in your section, stage and group Slack channels
- [ ] Ensure you've assigned tasks via PTO by Roots including assigning some tasks to a relevant #g_ , #s_ or #product slack channel
- [ ] Ensure your PTO by Roots auto-responder points team members to this issue
- [ ] Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#current-status) to include a reference to this issue

### :x: Closing Tasks
- [ ] Review the [Returning from Time Off ](https://about.gitlab.com/handbook/product/product-manager-role/#returning-from-time-off) guidance
- [ ] Assign back to yourself and remove others
- [ ] Review any Retrospective Items and update [this template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/PM-Coverage.md)
