## Overview

With every major release, it's important we [communicate breaking changes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communicating-breaking-changes) clearly and consistently to all of our users. This is especially important for SaaS customers, as the breaking changes may affect their workflow as early as the 1st day of a major release month, and continue to do so in a rolling fashion until the major release is formally announced on the 22nd day of the month. Follow the task list below to drive communication about breaking changes. 

**NOTE:** 

**- All due dates in this issue assume a regular release cycle of XX.0 - XX.10 --> XX.00, going from June to May** For example, if 15.0 is being released May 22, you'll be assigned this issue 4 milestones in advance, during 14.7. If minor releases are added or removed, you will need to shift the dates below to align.

## Tasks

Perform these tasks when you first open this issue. 

- [ ] Based on the planned date of the upcoming major release, update the due dates below.
- [ ] Find out when the code freeze of the milestone is and if there is a production lock
- [ ] In Slack `#release-post`, make an announcement informing Product Managers about the important days for this release. (This includes all the regular dates for the release post, but also code freeze. Any breaking change that will not be merged by the code freeze will need to be delayed by a year!) 
- [ ] Cross post this Slack thread across the following  channels: `#product #UX #development #quality #eng-managers `

### Due date XXXX-01-24 (by 24th day of the month, 4 milestones prior to major release)

- [ ] In Slack `#release-post`, make an announcement informing Product Managers that communication for [breaking changes](https://about.gitlab.com/handbook/product/gitlab-the-product/#breaking-changes-deprecations-and-removing-features) will commence in XX.8, leveraging broadcast messaging to point to [SSOT deprecation & removals in Docs](https://docs.gitlab.com/ee/update/deprecations).  
     - Remind them to follow the [deprecations and removals process](https://about.gitlab.com/handbook/marketing/blog/release-posts/#deprecations-removals-and-breaking-changes) and partner with [their Technical Writer](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments) to ensure docs are up to date at all times leading up to the major release.
     - Remind them to check the `removal_date` `breaking_change` keys in their deprecation and removal YML files, to ensure breaking changes are highlighted accurately in docs.
     - Link them to this issue in the post for reference.
     - [ ] Cross post this Slack thread across the following channels: 
          - [ ] `#product`
          - [ ] `#UX`
          - [ ] `#tw-team`
          - [ ] `#development`
          - [ ] `#quality`
          - [ ] `#eng-managers`
          - [ ] `#marketing`
          - [ ] `#product-marketing`
          - [ ] `#customer-success`
          - [ ] `#sales`
          - [ ] `#support_gitlab-com`
          - [ ] `#support_self-managed`
          - [ ] `#external-comms`

### Due date XXXX-02-20 (by the 20th day of the month, 3 milestones prior to major release)

- [ ] Follow the process for [requesting](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications/#requests-for-external-announcements) and [creating](https://about.gitlab.com/handbook/marketing/blog/#how-to-suggest-a-blog-post) a blog post and start one for breaking changes.
     - In the blog issue you create, communicate clearly that the URL for the blog post must be live/available on the first day of the major release milestone, for example, April 18 for a major release that's launching May 22 
     - For reference: the [issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11247) and [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/83586) for the 14.0 breaking-changes blog post.
     - Use the [14.0 breaking changes blog](https://about.gitlab.com/blog/2021/06/04/gitlab-moving-to-14-breaking-changes/) for reference, repurposing the intro content, formatting, etc. 
     - [ ] Add the blog issue you've created to **Related Issues** for this issue
     - [ ] In the blog issue you created, ping the head of Technical Writing `@susantacker` and ask her for a TW partner to help review the final blog post pre-publication 
- [ ] Follow the process for scheduling a GitLab.com [broadcast message]
(https://about.gitlab.com/handbook/product/product-processes/#gitlabcom-broadcast-messages-broadcast-messaging):
     - Create a [broadcast message issue](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=PM-in-app-messaging-request)
     - Use this copy for the broadcast message: `Major release XX.0 is coming up! There will be **breaking changes** in the release. Please visit [Deprecations and Removals](https://docs.gitlab.com/ee/update/deprecations) to learn more.`
     - In the broadcast message issue, communicate these requirements: 
          - The message initiates on the 18th of the month for milestone XX.9
          - After the user closes the message, it should not reappear again
     - Add the broadcast message issue you've created to **Related Issues** for this issue
- [ ] In Slack `#release-post`, announce to the Product Managers that broadcast messages will start running for breaking changes XX.9 onward. Remind them they need to make sure that [SSOT deprecations and  removals in Docs](https://docs.gitlab.com/ee/update/deprecations) is up to date as that's the source for automation. 
     - Link them to the broadcast message issue for reference.
     - [ ] Cross post this Slack thread across the following channels: 
          - [ ] `#product`
          - [ ] `#UX`
          - [ ] `#tw-team`
          - [ ] `#development`
          - [ ] `#quality`
          - [ ] `#eng-managers`
          - [ ] `#marketing`
          - [ ] `#product-marketing`
          - [ ] `#customer-success`
          - [ ] `#sales`
          - [ ] `#support_gitlab-com`
          - [ ] `#support_self-managed`
          - [ ] `#external-comms`

### Due date XXXX-03-20 (by the 20th day of the month, 2 milestone prior to major release)

- [ ] Do follow-ups as needed in your blog issue to ensure the link will go live on the first day of the major release milestone, for example, April 18 for a major release that's launching May 22. 
     - It's important the live URL be available by the 18th of the month as you'll schedule the broadcast message to start the 20th of the month, to ensure SaaS users are aware before breaking changes start getting merged and affect their workflow
- [ ] Check in with the VP of product `@adawar` as to whether there's any reason to not announce the breaking changes starting on the 20th of the major milestone month
     - There may sometimes be considerations and due to the nature of certain changes happening in the product, some teams may want to publish the blog till a later date
- [ ] Post a reminder to PMs in Slack #release-post to make sure all their breaking changes are appearing accurately in [Deprecations and Removals](https://docs.gitlab.com/ee/update/deprecations) as that'll be used as SSOT for broadcast messaging and auto-generating the breaking change blog.  
     - cross post in #product #eng-managers and #Tech-writers
- [ ] Follow the process for scheduling a GitLab.com [broadcast message](https://about.gitlab.com/handbook/product/product-processes/#gitlabcom-broadcast-messages-broadcast-messaging) 
     - Use this copy for the broadcast message: Major release XX.0 has launched! There are **breaking changes** in the release. Please visit [Title of breaking changes blog](link to live URL) and [Deprecations and Removals](https://docs.gitlab.com/ee/update/deprecations) to learn more.
     - In the broadcast message issue you create, communicate these requirements: 
          - message initiates on the 20th of the month for milestone XX.0
          - after the user has closed the message, it should not appear again till it re-initiates again during the following milestone
- [ ] In Slack #release-post, make an announcement informing Product Managers that final broadcast messages for breaking changes will run in XX.0 starting on the 20th of the month
     - Link them to the broadcast message issue for reference
     - [ ] Cross post this Slack thread across the following channels: 
          - [ ] `#product`
          - [ ] `#UX`
          - [ ] `#tw-team`
          - [ ] `#development`
          - [ ] `#quality`
          - [ ] `#eng-managers`
          - [ ] `#marketing`
          - [ ] `#product-marketing`
          - [ ] `#customer-success`
          - [ ] `#sales`
          - [ ] `#support_gitlab-com`
          - [ ] `#support_self-managed`
          - [ ] `#external-comms`

### Due date XXXX-04-01 (by the 1st day of the month, 1 milestone prior to major release)

- [ ] Work with Product Operations `@brhea` to populate the right list of breaking changes into the breaking changes blog MR
- [ ] In Slack #release-post share the MR/View app with the PM team and ask them to make sure none of their breaking changes are missing. 
     - Let them know they need to update along with a link to SSOT Docs in order for the breaking changes blog to update
     - Cross post in Slack #product #eng-managers #quality #docs
- [ ] Repeat the steps above again sometime around the 10th of the month by following up in the Slack thread you created above

### Due date XXXX-04-16 (by the 16th day of the month, 1 milestone prior to major release)

- [ ] Be sure your blog is getting published on time! 
- [ ] As soon as you have the live URL link, add it to the broadcast message issue you created for the major release

/label ~"Product Operations" ~"workflow::In dev"
/assign @fseifoddini @brhea
