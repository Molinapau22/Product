# Overview

The <%= FiscalDateHelper.new(Date.today).this_quarter %> iteration of Gitlab SaaS PNPS will be a series of email surveys, deployed as needed, with the intention of: 

1. Delivering an overall score by end of the quarter to track against our previous quarter score
1. Deliver scores by plan type, ensuring we get a sample that's representative of plan type breakdown
1. Backtrack and deliver a breakdown of Detractors plan types if needed
1. Maintaining response rate of ~3%
1. Reveal PNPS data into Sisense for viewing/tracking
  
**Requirements & Methodology:**

*  paid users gitlab.com
     * can include admins (payers) or users of the product
     * can include users with free access to paid features (such as educational institutions with Gold for free)
*  survey eligible users with the goal of achieving representative breakdown by plan type within +/- 5% accuracy
*  minimum 60 day subscription
*  aim for 95% confidence interval
*  no user should get a survey more than once a year

# Survey Questions:

We are using the standard Net Promoter Score questions:

How likely are you to recommend GitLab to a friend or colleague? (Scale 0 to 10 where 0 = Not at all likely and 10 = Very likely )
Why? (Open ended text field)

# Tools & Process: 

via Qualtrics

1. Pull a list of users for the quarter using [this query](https://app.periscopedata.com/app/gitlab:safe-dashboard/919244/Growth-UXR-Scratch?widget=12667049&udv=0). Those users are SaaS users on a paid plan (though that includes people getting a paid plan for free, like OSS/EDU).
1. Check the list against the list of emails sent in the previous quarters for the year, scrubbing anyone previously contacted.
1. Look up the overall SaaS percentage breakdown by plan. Our end goal will be to achieve a sample breakdown that roughly matches our population breakdown (-/+3%)
1. Send out waves of approximately 5000 emails each.
1. After each wave:
     - Pull a list of completed responses and calculate the current plan breakdown
     - Adjust the next wave to compensate for the current response breakdown, adding or subtracting the quantities of a plan for that wave as needed
1. Send out additional waves until we reach approximately 500 completed responses
1. Once we've reached our desired sample size,  pull a CSV and do the analysis in a Google Sheet.


[SaaS plan proportion source of truth](https://app.periscopedata.com/app/gitlab/444436/GitLab.com-Financial-KPIs?widget=5741517&udv=0)

[Q3 FY22 user list used](https://docs.google.com/spreadsheets/d/1sNIBrnH6YoYPCC9UIBZKMnbzAskXbfH0imLNVdu6iLI/edit?usp=sharing) `@alasch` 

[QX FY2X Raw analysis CSV Google sheet](https://docs.google.com/spreadsheets/d/1wA3YEM8YvNioSP5lhzTWCLiABxN7Ai48gFKg8SA2EcI/edit#gid=1308204053) `@alasch` 

`[QX FY2X all written responses list](ADD LINK)` `@alasch `

# Next Steps 

- [ ] Goals and requirements @fseifoddini
- [ ] Add due date to issue @fseifoddini 
- [ ] Add milestone to issue @fseifoddini 
- [ ] Add issue to appropriate epic `FYXX PNPS Surveys` @fseifoddini 
- [ ] Add due dates for the task lists below @fseifoddini and/or @alasch 
- [ ]  Update links to user list used, all written and raw data/analysis sheets in this issue description @alasch 
- [ ]  Launch surveys **month/day - month/day** @alasch 
- [ ]  Create analysis deck outline **month/day - month/day** @fseifoddini 
- [ ]  First analysis and draft report for UXR peer review **month/day - month/day** @alasch 
- [ ] First draft analysis and draft for Product Operations with 50 min live sync/walk thorough **month/day - month/day** @alasch 
- [ ]  Review and additional feedback (async) on analysis @fseifoddini **month/day - month/day**
- [ ]  Second draft and 25 min live sync (only if needed) **month/day - month/day** @alasch  
- [ ]  Deliver final changes, questions, requests (only if needed) **month/day - month/day** @fseifoddini 
- [ ]  Final version deck complete (async) **month/day - month/day** @alasch
- [ ]  Share out for feedback across teams: Slack #product, #UX, #Development, #Quality #Sales, #Customer-success, #Marketing #product-marketing @fseifoddini 
- [ ]  Add analysis slides to [prod ops direction page](https://about.gitlab.com/direction/product-operations/) @fseifoddini 
- [ ] Add link to analysis slides to read-only section of PM biweekly meeting @fseifoddini 

/label ~"workflow::In dev" ~"Product Operations" ~"prodops:direction" ~"prodops:feedbackloops"
/confidential

/assign @alasch @fseifoddini
