Hello Product Leads-- Please take the time to sign up, coordinate and reach out to <%= FiscalDateHelper.new(Date.today).this_quarter %> PNPS users who opted to chat with us! Please note this list is <%= FiscalDateHelper.new(Date.today).this_quarter %> paid SaaS users only. Readout deck `[here](ADD LINK)`. 

You can sign up (or assign your GPMs or PMs) using `[QX PNPS Followup Users](ADD LINK)`. Please note restricted access to only `product@gitlab.com` and `timtams@gitlab.com` due to privacy of user contacts. 

The process to follow is the same as last quarter and can be found in the product handbook: [PNPS Responder Outreach](https://about.gitlab.com/handbook/product/product-processes/#pnps-responder-outreach). Please note we ask that you add the label `PNPS Improvement` to any epics/issues that result from the PNPS analysis and/or PNPS outreach calls.  

**Important note**: We do not want to overwhelm users with multiple contacts. If the person you want to talk to has already been signed up for by someone else for outreach, coordinate to join your questions and/or outreach session. Thank you!

/label ~"workflow::In dev" ~"Product Operations" ~"prodops:direction" ~"prodops:feedbackloops"
/assign @fseifoddini @brhea
/confidential

*To Do (for Product Operations only)*

- [ ] Update all document links in issue description
- [ ] Add due date: 5th day of last month of the current fiscal quarter
- [ ] Add milestone
- [ ] Assign to appropriate epic `FYXX PNPS Surveys`
- [ ]Tag in @gl-product-leadership to action
- [ ] Tag in @spatching to action TAMs
- [ ] 2 weeks prior to due date:
     - [ ] Change workflow label to `workflow::inreview`
     - [ ] Check user list to see if team has actioned sufficiently, and if not, ping product leads to action
- [ ] Close issue on due date
