Data collection period: **month/day - month/day** – **month/day - month/day**

[Slides]() @brhea

[Raw analysis spreadsheet]() @alasch

## Opening tasks

- [ ] Update collection period  dates above @fseifoddini
- [ ] Update due dates of the task list below @fseifoddini
- [ ] Assign a due date to this issue @fseifoddini
- [ ] Assign due dates to task list items below @fseifoddini

## Survey To Dos

- [ ] By **month/day - month/day**: Create analysis slides template, update slides owned by Prod Ops, and paste link above @brhea
    - Save this to [PPS survey folder](https://drive.google.com/drive/folders/1LPa_4qaSW8NuKA90yIapeD2ztNyM0cNH) on gdrive
    - Give access to the appropriate collaborators
- [ ] By **month/day - month/day**: Export and link raw data sheet above @alasch
     - Save this to [PPS survey folder](https://drive.google.com/drive/folders/1LPa_4qaSW8NuKA90yIapeD2ztNyM0cNH) on gdrive
     - Give access to the appropriate collaborators
- [ ] By **month/day - month/day**: Complete feature count slides: @alasch
- [ ] By **month/day - month/day**: Complete ARR slides @dpeterson1
- [ ] By **month/day - month/day**: Complete ARR slides @dpeterson1
- [ ] **month/day - month/day** - **month/day - month/day**:  Review and wrap up of all slides @brhea (ping @alasch @dpeterson1 if needed)
- [ ] By **month/day - month/day**: Final review @fseifoddini
- [ ] By **month/day - month/day**: Share out with Product, UX, Dev, Sales, CS, Marketing, Product Marketing, Quality Assurance by posting in Slack @brhea
     - CC  `@fseifoddini @alasch and @dpeterson1` in the Slack post
- [ ] After x-posting above has happened, share out in  Slack CEO channel @fseifoddini 

**Tasks below should be completed within two weeks of sharing out the analysis across teams:** 

- [ ] Update `features.yml` / [feature by theme page](https://about.gitlab.com/features/by-theme/) with the latest feature counts, following [these instructions](https://about.gitlab.com/handbook/product/product-operations/updating-pps-data/)  @brhea
- [ ] Random drawing: @alasch
- [ ] Send prize to winner: @alasch
- [ ] Add link to analysis slides to  [prod ops survey page](https://about.gitlab.com/handbook/product/product-operations/surveys/) @brhea
- [ ] Add link to analysis slides to read-only section of PM biweekly meeting

/label ~"workflow::In dev" ~"Product Operations" ~"prodops:direction" ~"prodops:feedbackloops"
/confidential
/assign @brhea @fseifoddini @dpeterson1 @alasch
