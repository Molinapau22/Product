QX PPS update was completed per this `[FYXX QX PPS Feature list Comparision](ADD URL)`

* [ ] By **month/day**: Activate managers to review and make sure [features source of truth](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/features.yml)/ [feature by tier page](https://about.gitlab.com/features/by-paid-tier/) are up to date `@fseifoddini`  
* [ ] Create FYXX QX PPS Feature list Comparision and add link `[here](ADD LINK)` `@fseifoddini @brhea`
* [ ] Update the survey feature lists in Qualtrics based on FYXX QX PPS Feature list  `@alasch @brhea` 

/label ~"workflow::In dev" ~"Product Operations"
/assign @brhea @fseifoddini @alasch 
