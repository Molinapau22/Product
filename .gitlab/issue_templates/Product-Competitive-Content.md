<!-- We are always looking for ways to effectively share learnings across Product, Design, Engineering and Product Marketing. This template serves as a method of collecting insights from competitive analysis for distribution.>

## Description
<!-- Describe the competitive content, report, or finding.-->

## Assets
<!-- Update the links below as they are available -->
* [Report]()
* [Related Direction Page]()
* Add follow up issues added as related issues

## Tasks
### Opening tasks
* [ ] Add correct labels for `~devops::` `~group::` `~section:`
* [ ] Add a `:recycle: Retrospective Thread` to make improvements to this template 

### Distill findings from Report
* [ ] Read the report or findings 
* [ ] Identify 3-5 (or more) key takeaways from report
* [ ] Create a merge request to document the takeaways on relevant direction pages 

### Share direction page updates 
* [ ] Post direction page merge requests in: 
     * [ ] #s_relevant stage 
     * [ ] #g_relevant groups
     * [ ] #product
     * [ ] #analyst_relations 

### Feature enhancements 
* [ ] Create follow up issues and ping relevant product managers
* [ ] Attach issues as related to this one

### Closing tasks 
* [ ] Update [this template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Product-Competitive-Content.md) with any improvements suggested in the Retrospective Thread
* [ ] Close this issue

/label ~competitive ~product 
/assign `@PM` 
