## Overview

I wanted to provide some updates on the Performance Indicators for `Stage::Group` in preparation for the next Section PI Review.

Link to [Last month's update]()

## What we track

As you are aware we track several metrics in order to understand customer usage and adoption of features:

* The leading indicator for the group AKA Performance Indicator (PI) is `insert metric here`
* Another indicator tracked is `metric 2`

## :question: How are we doing?

### Current goal
Link to [PI page]()

`Graph or link to dashboard`

## Insights

Possible entries here:
 - Hypothesis about why things changed
 - Reactions to previous hypothesis

## What is next?

Possible items for discussion
 - New/modified features intended to increase the PI
 - UX goals to increase the PI
 - Additional tracking for existing features

## What is the next goal?

Current Goal and progress
Next Goal

## Links/reference

<-- Label Reminder: Be sure to notify your manager and engineering manager by '@'ing them in the cc below -->

/cc `@manager` `@em`
