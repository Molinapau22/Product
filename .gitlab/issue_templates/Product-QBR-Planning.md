## :dart: Intent
To coordinate attendance, and document and disseminate key insights from Product Leaders from QBRs happening in early FYXX-QX.

## Tasks
- [ ] Signup for attendance and ensure coverage on critical QBRs
- [ ] Send reminder to Product Leadership 7 days prior to QBRs - `Add Volunteer`
- [ ] Using Product QBR Common Notes Template in Google create notes docs and add session titles ordered by Date and Time - `Add Volunteer`
- [ ] Add highlights and takeaways to a [common notes doc](https://docs.google.com/document/d/10l1uR4w1qKT7z4Wsrlec5875gYdfuBybArgRlb_-irE/edit#)
- [ ] Summarize key takeaways and document in the handbook - `Add Volunteer`
- [ ] Present in product meetings - `Add Volunteer`
- [ ] Retro thread - `Add Volunteer`
