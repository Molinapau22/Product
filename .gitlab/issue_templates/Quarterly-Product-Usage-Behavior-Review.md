## :dart: Intent
The intent of this issue is to:
- review and update if needed any existing product usage behavior metrics in the handbook
- consider adding additional product usage behavior metrics

## :book: References
- [Handbook: Known Product Usage Behaviors](https://about.gitlab.com/handbook/product/gitlab-the-product/#known-product-usage-behaviors)

## Tasks

### :wave: Open
* [ ] Create retrospective Thread - @kencjohnston

### :pencil2: Review Actions
Actions for review
- Ping relevant/assigned PMs to review their individual metrics
- Ping product leadership to consider additional metrics

### :heavy_check_mark: Review Complete
- [ ] Individual metrics updated
- [ ] New Metrics considered

### :x: Close
* [ ] Post a Highlights Thread and share it in Development and Product channels
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Quarterly-Product-Usage-Behavior-Review.md) from the Retro Thread

/assign @kencjohnston

/due 28 days
