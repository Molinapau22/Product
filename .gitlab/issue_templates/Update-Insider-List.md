# To Do

- [ ] By the 10th: Check in with `@david` and `@justinfarris` to confirm if any changes are needed `@fseifoddini`
- [ ] By the 20th: Send final list revisions to Legal if needed `@fseifoddini`
- [ ] By the 28th: Review final list from Legal `@fseifoddini @david @justinfarris`

Reference Docs (limited access) :disappointed:
https://docs.google.com/spreadsheets/d/1abKlfqu8w6uF7Ek2wjoVaQIBo0RV7Wxakb62Fom-Ilo/edit#gid=1513348970

/assign @fseifoddini @david @justinfarris
