## Intent
Ensure I'm contacting 3 customers a week on my way to 100 customers a year.

## References
- [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?label_name[]=customer-contact)
- [Current NPS Survey Response Sheet ](https://docs.google.com/spreadsheets/d/1HKiwkf-j_vHPPcYU6ouKZKzvMtPAUx6QdaFzPER15rc/edit#gid=132919894)
- Historical NPS Survey Response Sheets - [Q2](https://docs.google.com/spreadsheets/d/1mIt52e7hP-mpn5N6l7emx4qNW8GbTtHYCwlviYhLIjw/edit#gid=1082605413)([Q1](https://docs.google.com/spreadsheets/d/1Lw_bbTz4K15H8ClFMGO6tiQXrKgCL3Lf_yDAiNOXLy4/edit#gid=0)/[Q4](https://docs.google.com/spreadsheets/d/1mwps_sDbBeu9DnH-MCs2okvyYlli6HKb3mniMuVXwyU/edit#gid=0))

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Consider whether you need another source
* [ ] If first week of the month - Consider increasing the number of schedule outreach to ensure you are hitting goal
* [ ] If first week of the quarter - Consider additional sources of feedback

### :pencil2: Review List
* [ ] Review NPS Survey Response Sheets

### :calendar: Schedule
Using this template and [Markdown Here](https://chrome.google.com/webstore/detail/markdown-here/elifhakcjgalahccnjkneoccemfahfoa?hl=en)
```
👋  Hi!

You recently completed a GitLab NPS survey and left open the option of someone from GitLab contacting you as a follow-up. Well guess what?! I'm [that person](https://gitlab.com/kencjohnston). I'd love to set up a time to chat. If you are up for it you can respond to this email or use my [Calendly link](https://calendly.com/gitlab-kjohnston) to book a time that works for you. 

Thanks!
```
* [ ] Send :six: email requests with Calendly invite


### :thinking: Reflect
* [ ] Review [notes from previous conversations](https://docs.google.com/document/d/1HrkMtl27EJV4ofimVwWtwVfRErShenv8TTHSljkf-8c/edit)
* [ ] Update status in NPS Survey Respondent sheet
* [ ] Compile docs and issue links and respond to customers via email
* [ ] Think about conversations you've had with customers any broad themes?

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Customer-Contact-Reminder-Kenny.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" 

/confidential

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338
