## :dart: Intent
Ensure I'm focusing my time and team on the most important things week to week.

## :book: Refrences
- [Priorities.md](https://gitlab.com/kencjohnston/kencjohnston/-/blob/master/Priorities.md#)
- [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&assignee_username%5B%5D=kencjohnston&label_name%5B%5D=Weekly%20Priorities)
## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Create issues for any un-accounted for [Quarterly Priority](https://gitlab.com/kencjohnston/kencjohnston/-/blob/master/Priorities.md#quarterly-priorities) actions
* [ ] If first week of the month - Review all Open issues and MRs for delegation
* [ ] If first week of the quarter - Review and adjust [Quarterly Priority](https://gitlab.com/kencjohnston/kencjohnston/-/blob/master/Priorities.md#quarterly-priorities) - set and track quarterly goals

### :envelope: Monday Tasks
* [ ] Review Ops [Dogfooding Board](https://gitlab.com/groups/gitlab-org/-/boards/1212116?label_name%5B%5D=Dogfooding&label_name%5B%5D=section::ops) ([Handbook](https://about.gitlab.com/handbook/product/product-processes/#dogfooding-process))
* [ ] Review Ops [Infra-Fin Board](https://gitlab.com/groups/gitlab-com/-/boards/992233?label_name%5B%5D=infrafin)
* [ ] Review Ops [UX Debt](https://gitlab.com/groups/gitlab-org/-/boards/3670333?label_name[]=UX%20debt&label_name[]=section%3A%3Aops)

### :pencil2: Review
* [ ] Cleanout `Open` issues on [board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston)
* [ ] Review [Quarterly Priorities](https://gitlab.com/kencjohnston/kencjohnston/-/blob/master/Priorities.md#quarterly-priorities), [Calendar](https://calendar.google.com/calendar/u/0/r/week), [gitlab-com Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1353560?assignee_username=kencjohnston), [Top Initiatives Issue Board](https://gitlab.com/groups/gitlab-com-top-initiatives/-/boards/3771251?assignee_username=kencjohnston) and [Issue List](https://gitlab.com/dashboard/issues?assignee_username=kencjohnston)
* [ ] Schedule Time Blocks for Priorities
* [ ] Edit [Weekly Priorities](https://gitlab.com/kencjohnston/kencjohnston/-/edit/master/Priorities.md)

### :mega: Communicate
* [ ] Communicate them in Slack (on threads in #product and #ops-section and DM to David)

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-Priority-Setting-Kenny.md) from the Retro Thread

/assign @kencjohnston

/label ~"section::ops" ~"Weekly Priorities"

/epic https://gitlab.com/groups/gitlab-com/-/epics/1338
