## :dart: Intent
Ensure I'm collecting a SaaS Free User Async Update Each Week

## :book: Refrences
- [Direcation]()
- [Async Update Process]()
- [DRI Table](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#sub-project-dris)

## Request Template

```
@DRI - Please remember to provide your [async weekly update](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#weekly-async-updates) here by 20:00UTC (12:00PST) tomorrow. Thanks!
```

## Summary Template
> 
> # Weekly SaaS Free User Efficiency Initiative Update
>
> ## :tada: Highlights
> - Highlights - What key results and lessons learned were accomplished
>
> ## :warning: Risks
>- Risks - What current risks are we facing in achieving our goal
>
> ## :key: Key Free User Offer Adjustment Sub-Project Status
> - Storage Limits Enforcement - ⚠ Targeting Free Visibiliity April 30
> - Storage & User Limits Comms - ✅ Separated User Limits Blog Post mid-March
> 
> ## :chart_with_upwards_trend: Key Metrics Status
> Updated as of XXX-XX-XX. Last, Current and Next month indicators. 
> - Cost Projection
>   - Last Month
>    - Current Month
>    - Next Month
> - Conversion Projection 

## Tasks

### :wave: Open
* [ ] Create retrospective Thread

### :exclamation: Extras
* [ ] If first week of the month - Ask additional probing questions when requesting updates

### :envelope: Monday Tasks
* [ ] Open [all artifacts in the DRI Table](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#sub-project-dris) and use request template to request an update (Except Community Programs - Tanya's got it)
* [ ] Review [Key Performance Indicators](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#key-performance-indicators) and Sub-project success metrics.


### :pencil2: Tuesday Tasks
* [ ] Open [all artifacts](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#sub-project-dris) and review and summarize in comment on [the Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/free-saas-user-efficiency/-/epics/1) and ping `FYI @gl-free-saas-efficiency-leaders @david @sloyd`
* [ ] Update Highlights and Pending Decisions content

### :mega: Communicate
* [ ] Communicate the async update in Slack (ping ahead of the Wednesday Sync review

### :x: Close
* [ ] Updates to [this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Weekly-SaaS-Free-User-Async-Update.md) from the Retro Thread

/assign @kencjohnston
