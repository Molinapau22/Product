## Introduction 

Two sub-values at GitLab are [sharing](https://about.gitlab.com/handbook/values/#share) and [findability](https://about.gitlab.com/handbook/values/#findability). In the Product Management organization, we strive to transparently communicate where we are headed and celebrate recent achievement in our Product Direction. The [Product Direction Showcase]() is a monthly meeting featuring 4 stage groups. 

## Opening Issue Tasks 

- [ ] Update title with month - @jreporter 
- [ ] Add retrospective thread - @jreporter 
- [ ] Confirm assignees are stage leaders - @jreporter 
- [ ] Set due date for 1 week before Product Direction Showcase - @jreporter 
- [ ] Confirm meeting is scheduled with Zoom 
- [ ] Update [Agenda](https://docs.google.com/document/d/13BuRLiDJpNNhGmO1dOPqMQR0yaEDZY4CFGFR49tZMYo/edit?usp=sharing) 

## Actions to take 

- [ ] Add your stage to a table slot below 
- [ ] Add deep dive topic for stage and tag Product Manager 
- [ ] Add links to direction page or deck 

### Stage order table 

| Order | Stage | Direction DRI | Deep Dive DRI  | Deep Dive Topic  | Links  |
| --- | --- | --- | --- | --- | --- |
| 1 |  |  |  |  |  |
| 2 |  |  |  |  |  |
| 3 |  |  |  |  |  |
| 4 |  |  |  |  |  |

## Closing Issue Tasks 

- [ ] Resolve any retrospective items by updating issue template 
- [ ] Share recording in #product, #whats-happening-at-gitlab 
- [ ] Close Issue 

/assign @jreporter @kbychu @sarahwalnder @ogolowinski @fzimmer @mushakov @hbenson @jennifergarcia20
/due in 14 days 
