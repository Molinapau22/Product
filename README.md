# GitLab Product Department Repository
* [Handbook](https://about.gitlab.com/handbook/product/)

## GitLab Product Process

This repository also includes code to automate the scheduled creation of issues for the Product team.

## Usage

Add a new item to [issues.yml](https://gitlab.com/gitlab-com/Product/-/blob/main/config/issues.yml) and [sections.yml (optional)](https://gitlab.com/gitlab-com/Product/-/blob/main/config/sections.yml)


| Name | Values | Description |
| ----------- | ----------- | ----------- |
| name | `String` | Inserted into the title of the generated issue. |
| scope | `all`, `section` | Determines at what level the issue will be created.  |
| template | `String` | Inserted as the description of the generated issue. Will parse template in `/templates` (for `section` scoped) or `/.gitlab/issue_templates` (for `all` scoped) directory and render as markdown |
| day_of_week (optional) | `Sun`, `Mon`, `Tue`, `Wed`, `Thu`, `Fri`, `Sat` | If present, will run weekly on the given value. |
| day_of_month (optional) | `Int` | If present, automation will run on the day of the month of the given value. |
| day_of_year (optional) | `Array` | If present, automation will run on the day of the year of the given value(s). |
| frequency (optional) | `String` | If present, overrides the default frequency (based on `day_of_*`) that is added to the issue title. |

### Day of Week

```yml
- name: 'Weekly Priority Setting - Kenny'
  day_of_week: Sun
  scope: all
  template: Weekly-Priority-Setting-Kenny.md
```

### Day of Month

```yml
- name: 'Ops Section Direction Updates'
  day_of_month: 18
  scope: all
  template: Ops-Section-Direction-Updates.md
```

### Day of Year (override "Yearly" with "Biannually")

```yml
- name: 'Product Milestone Creation'
  day_of_year: [30, 210]
  scope: all
  template: 'Create-Milestones.md'
  frequency: 'Biannually'
```

## Testing Locally
To test the create_issues script use the `--dry-run` arguement. Before running, make sure you set the `CI_PROJECT_ID` (for the product project, it's 1641463) and the `GITLAB_API_PRIVATE_TOKEN` environment variables.

```
bundle exec ruby create_issues.rb --dry-run
```

## Testing YAML Syntax

YAML can be hard. You can check the YAML file syntax locally with these commands:

First install YAMLlint
```
gem install yamllint
```

And once installed testing the two primary yaml files
```
yamllint config/issues.yml config/sections.yml
```

## Limitations

* The only scopes that are available are `section` and `all`. We should consider adding `group` that would allow for the creation of an issue per product group. 
* In order to use multiple quick actions you have to add an extra line space between them
