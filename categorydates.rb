require 'yaml'
require "open-uri"

yaml_content = open("https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/categories.yml"){|f| f.read}
categories = YAML::load(yaml_content)

# set this true to show ok messages (for debugging)
show_oks = false

global_error_state = false

categories.each { |category|
  has_error = false
  output = ""

  name = category[1]["name"]
  stage = category[1]["stage"]
  
  current_maturity = category[1]["maturity"]
  # next line is to deal with calling minimal and available two different names
  current_maturity = "available" if current_maturity == "minimal"

  available_date = category[1]["available"]
  viable_date = category[1]["viable"]
  complete_date = category[1]["complete"]
  lovable_date = category[1]["lovable"]
 
  if current_maturity
    # only evaluate items that have a maturity
    output += "#{name}\n"
    output += "  category is currently at #{current_maturity} maturity (stage: #{stage})\n"
    current_maturity == "planned" ? caught_up = true : caught_up = false
    if current_maturity == "planned" && !category[1]["available"]
      output += "  ERROR: Feature is planned but has no available date\n"
      global_error_state = true
      has_error = true
    end
    ["available","viable","complete","lovable"].each { |maturity|
      output += "  Checking #{maturity} (#{category[1][maturity]})\n" if show_oks
      if caught_up
        # we have caught up, maturities from this point should be in the future or unset
        if !category[1][maturity]
          output += "    OK: Is unset as expected\n" if show_oks
        elsif category[1][maturity] < Date.today
          output += "    ERROR: Expected date in the future for #{maturity} but found #{category[1][maturity]}\n"
          global_error_state = true
          has_error = true
        else
          output += "    OK: Is in future as expected\n" if show_oks
        end
      else
        # we have not yet caught up to current maturity, so maturities should be in the past
        if !category[1][maturity]
          output += "    ERROR: Expected date to be set for #{maturity} but it was not\n"
          global_error_state = true
          has_error = true
        elsif category[1][maturity] > Date.today
          output += "    ERROR: Expected date in the past for #{maturity} but found #{category[1][maturity]}\n"
          global_error_state = true
          has_error = true
        else
          output += "    OK: Is in past as expected\n" if show_oks
        end
      end
      caught_up = true if current_maturity == maturity 
    }
  end

  print output if has_error == true || show_oks == true
}

exit 1 if global_error_state
